package com.irca.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.irca.cosmo.R;
import com.irca.dto.ItemMods;

import java.util.List;

public class ItemModsAdapter extends RecyclerView.Adapter<ItemModsAdapter.MultiSelectDialogViewHolder> {

    private List<ItemMods> itemCartDtoList;
    private Activity mContext;

    public ItemModsAdapter(List<ItemMods> dataSet, Activity context) {
        itemCartDtoList = dataSet;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ItemModsAdapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_mods, parent, false);
        return new ItemModsAdapter.MultiSelectDialogViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ItemModsAdapter.MultiSelectDialogViewHolder holder, int position) {
        try {
            holder.itemTextView.setText(itemCartDtoList.get(position).getItemName());


            if (itemCartDtoList.get(position).getModifier() != null && !itemCartDtoList.get(position).getModifier().isEmpty() && !itemCartDtoList.get(position).getModifier().equals("null"))
            {   holder.modsTextView.setVisibility(View.VISIBLE);
                holder.label.setVisibility(View.VISIBLE);
                holder.modsTextView.setText(itemCartDtoList.get(position).getModifier());
                holder.label.setText("Modifier");
            }
              if (itemCartDtoList.get(position).getAllergen() != null && !itemCartDtoList.get(position).getAllergen().isEmpty() && !itemCartDtoList.get(position).getAllergen().equals("null"))
             {    holder.Allergen.setVisibility(View.VISIBLE);
                 holder.text_viewAllergen.setVisibility(View.VISIBLE);
                holder.text_viewAllergen.setText(itemCartDtoList.get(position).getAllergen());
                holder.Allergen.setText("Allergen");

            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder {

        private TextView modsTextView, itemTextView,label,Allergen,text_viewAllergen;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            itemTextView = view.findViewById(R.id.text_viewItem);
            modsTextView = view.findViewById(R.id.text_viewMods);
            label = view.findViewById(R.id.label);
            Allergen = view.findViewById(R.id.Allergen);
            text_viewAllergen = view.findViewById(R.id.text_viewAllergen);
            modsTextView.setVisibility(View.GONE);
            label.setVisibility(View.GONE);
            Allergen.setVisibility(View.GONE);
            text_viewAllergen.setVisibility(View.GONE);
        }


    }


}

