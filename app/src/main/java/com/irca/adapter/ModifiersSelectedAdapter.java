package com.irca.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.dto.ModifiersDto;
import com.irca.fields.OnRecyclerViewItemClickListener;

import java.util.List;

public class ModifiersSelectedAdapter extends RecyclerView.Adapter<ModifiersSelectedAdapter.MultiSelectDialogViewHolder> {

    public List<ModifiersDto> dataSet;
    private Activity mContext;
    private OnRecyclerViewItemClickListener<ModifiersSelectedAdapter.MultiSelectDialogViewHolder, ModifiersDto> onRecyclerViewItemClickListener;

    public ModifiersSelectedAdapter(List<ModifiersDto> dataSet, Activity context) {
        this.dataSet = dataSet;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ModifiersSelectedAdapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_mods, parent, false);
        return new ModifiersSelectedAdapter.MultiSelectDialogViewHolder(view);
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<ModifiersSelectedAdapter.MultiSelectDialogViewHolder, ModifiersDto> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ModifiersSelectedAdapter.MultiSelectDialogViewHolder holder, int position) {
        try {
            if (dataSet.get(position).getFreeFlow().isEmpty()) {
                holder.nameTextView.setText(dataSet.get(position).getModifierName() + " " + dataSet.get(position).getModifierDetailName());
            } else {
                holder.nameTextView.setText(dataSet.get(position).getFreeFlow());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView nameTextView;
        LinearLayout linearLayout;

        MultiSelectDialogViewHolder(View view) {
            super(view);

            nameTextView = view.findViewById(R.id.text_viewMemberName);
            linearLayout = view.findViewById(R.id.linear_layoutMember);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(final View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, dataSet.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

}


