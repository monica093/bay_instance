package com.irca.CustomAdapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.fields.ItemPromo;

import java.util.List;

/**
 * Created by Manoch Richard on 27-Mar-18.
 */

public class ItemPromotionAdapter extends RecyclerView.Adapter<ItemPromotionAdapter.MyViewHolder> {
    private List<ItemPromo> promotionList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre, billnumber;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            //genre = (TextView) view.findViewById(R.id.genre);
            //year = (TextView) view.findViewById(R.id.year);
            //billnumber = (TextView) view.findViewById(R.id.txt_billnumber);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sales_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemPromotionAdapter.MyViewHolder holder, int position) {
        ItemPromo ipromo = promotionList.get(position);
        holder.title.setText(ipromo.getPromotionName());
        //holder.genre.setText("Free ItemName:  " + movie.getCustomername());
        //holder.year.setText("Free qty:" + movie.getCustomeramount());
        //holder.billnumber.setText(":"+movie.getBillnumber());
    }

    public ItemPromotionAdapter(List<ItemPromo> _promotionList) {
        this.promotionList = _promotionList;

    }

    @Override
    public int getItemCount() {
        return promotionList.size();
    }
}
