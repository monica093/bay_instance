package com.irca.CustomAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.db.Dbase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manoch Richard on 28-Jan-18.
 */

public class WaiterAdapter extends BaseAdapter implements Filterable {

    List<String> masterStore;
    Context context;
    WaiterFilter waiterFilter = null;
    Dbase dbase = null;

    public WaiterAdapter(Context c, List<String> masterStoreArrayAdapter) {
        this.masterStore = masterStoreArrayAdapter;
        context = c;
    }
    public void updateList(List<String> list){
        this.masterStore = list;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return masterStore.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_storelist1, parent, false);

        }


        TextView storeName = (TextView) convertView.findViewById(R.id.storeText);
        storeName.setText(masterStore.get(position).toString());


        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (waiterFilter == null) {
            waiterFilter = new WaiterFilter();
        }
        return waiterFilter;
    }

    private class WaiterFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            if (charSequence == null || charSequence.length() == 0) {
                results.values = masterStore;
                results.count = masterStore.size();
            } else {
                dbase = new Dbase(context);
                results.values = dbase.getWaiterFilter(charSequence.toString());
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults.values != null) {
                masterStore = (List<String>) filterResults.values;
                notifyDataSetChanged();
            } else {
                masterStore = new ArrayList<>();
            }

        }
    }
}
