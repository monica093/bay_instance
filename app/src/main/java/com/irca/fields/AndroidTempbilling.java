package com.irca.fields;

/**
 * Created by Manoch Richard on 10/23/2015.
 */
public class AndroidTempbilling
{

    float Tamount;
    public String memberId;
    public String tableId;
    public String storeId;
    public String narration;
    public String Dependantid;

    public String getPaxcount() {
        return paxcount;
    }

    public void setPaxcount(String paxcount) {
        this.paxcount = paxcount;
    }

    public String paxcount;

    public String getDependantid() {
        return Dependantid;
    }

    public void setDependantid(String dependantid) {
        Dependantid = dependantid;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }



    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getMemberId() {
        return memberId;
    }

    public String getTableId() {
        return tableId;
    }


    public float getTamount() {
        return Tamount;
    }

    public void setTamount(float tamount)
    {
        Tamount = tamount;
    }

}
