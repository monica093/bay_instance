package com.irca.fields;

/**
 * Created by Manoch Richard on 25-Apr-16.
 */
public class Card {

    public String cardtype;


    public String personid;
    public String FirstName;
    public String personcode;
    public String dummy_account;
    public String account;
    public String billmode;

    public String tableNO;

    public String getPaxcount() {
        return paxcount;
    }

    public void setPaxcount(String paxcount) {
        this.paxcount = paxcount;
    }

    public String paxcount;

    public String getSteward() {
        return steward;
    }

    public void setSteward(String steward) {
        this.steward = steward;
    }

    public String steward;

    public String getOtdate() {
        return otdate;
    }

    public void setOtdate(String otdate) {
        this.otdate = otdate;
    }

    public String otdate;

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public String getPersoncode() {
        return personcode;
    }

    public void setPersoncode(String personcode) {
        this.personcode = personcode;
    }

    public String getTableNO() {
        return tableNO;
    }

    public void setTableNO(String tableNO) {
        this.tableNO = tableNO;
    }

    public String getBillmode() {
        return billmode;
    }

    public void setBillmode(String billmode) {
        this.billmode = billmode;
    }

    public Card() {

    }


    public String getDummy_account() {
        return dummy_account;
    }

    public void setDummy_account(String dummy_account) {
        this.dummy_account = dummy_account;
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }


    public String getpersonid() {
        return personid;
    }

    public void setpersonid(String Personid) {
        personid = Personid;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getpersoncode() {
        return personcode;
    }

    public void setpersoncode(String Personcode) {
        personcode = Personcode;
    }

}
