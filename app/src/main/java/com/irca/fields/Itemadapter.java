package com.irca.fields;

/**
 * Created by Shwetha on 24-Nov-16.
 */
public class Itemadapter {



    public String itemName;
    public String itemCode;
    public String quantity;
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }




}
