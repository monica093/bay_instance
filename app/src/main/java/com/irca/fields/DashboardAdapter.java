package com.irca.fields;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.irca.cosmo.R;

import java.util.List;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {

    private Context context;
    private List<DashboardDto> stringList;
    private OnRecyclerViewItemClickListener<DashboardAdapter.MyViewHolder, DashboardDto> onRecyclerViewItemClickListener;

    public DashboardAdapter(List<DashboardDto> stringList, Context context) {
        this.stringList = stringList;
        this.context = context;
    }

    @NonNull
    @Override
    public DashboardAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        try {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_dashboard_item, parent, false);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return new DashboardAdapter.MyViewHolder(view);
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<DashboardAdapter.MyViewHolder, DashboardDto> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull DashboardAdapter.MyViewHolder holder, int position) {
        try {
            DashboardDto dashboardDto = stringList.get(position);
            holder.titleTextView.setText(dashboardDto.getTitle());
            holder.imageView.setImageResource(dashboardDto.getImage());
            holder.bLinearLayout.setBackgroundColor(context.getResources().getColor(dashboardDto.getColor()));
            holder.iLinearLayout.setBackgroundColor(context.getResources().getColor(dashboardDto.getColor1()));

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        try {
            return stringList.size();
        } catch (Throwable e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView titleTextView;
        ImageView imageView;
        LinearLayout bLinearLayout, iLinearLayout;

        MyViewHolder(View itemView) {
            super(itemView);
            try {
                imageView = itemView.findViewById(R.id.imageView);
                titleTextView = itemView.findViewById(R.id.textView);
                bLinearLayout = itemView.findViewById(R.id.linearLayoutB);
                iLinearLayout = itemView.findViewById(R.id.linearLayoutI);
                itemView.setOnClickListener(this);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        //        @Override
//        public void onClick(View view) {
//            try {
//                DashboardDto dashboardDto = stringList.get(getAdapterPosition());
//                switch (dashboardDto.getId()) {
//                    case "1":
//                        Intent i1 = new Intent(context, DashboardBillingInfo.class);
//                        i1.putExtra("accNO", context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE).getString("ac_no", "0"));
//                        i1.putExtra("Item", 0);
//                        context.startActivity(i1);
//                        break;
//                    case "2":
//                        break;
//
//                }
//            } catch (Throwable e) {
//                e.printStackTrace();
//            }
//        }
        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, stringList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}

