package com.irca.fields;

import java.io.Serializable;

public class DashboardDto implements Serializable {

    public String id, title;
    public int image, color, color1;

    public DashboardDto(String id, String title, int image, int color,int color1) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.color = color;
        this.color1 = color1;
    }

    public int getColor1() {
        return color1;
    }

    public void setColor1(int color1) {
        this.color1 = color1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

}
