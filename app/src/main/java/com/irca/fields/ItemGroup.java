package com.irca.fields;

/**
 * Created by Manoch Richard on 10/23/2015.
 */
public class ItemGroup {
    public String getGroupid() {
        return Groupid;
    }

    public void setGroupid(String groupid) {
        Groupid = groupid;
    }

    //public int Groupid;
    public String  Groupid;
    public  String Groupname,Groupcode;
    public ItemGroup(){};



    public String getGroupname() {
        return Groupname;
    }

    public void setGroupname(String groupname) {
        Groupname = groupname;
    }

    public String getGroupcode() {
        return Groupcode;
    }

    public void setGroupcode(String groupcode) {
        Groupcode = groupcode;
    }
}
