package com.irca.Billing;

/**
 * Created by Monica on 7/23/2018.
 */

public class Taxdataexcem {
    public String BillID;
    public String TaxID;
    public String TaxTypeName;
    public String Value;
    public String TaxAmount;
    public String Rate;
    public String Description;
    public String TaxSubTypeCode;
    public String IsSubTax;

    public String getBillID() {
        return BillID;
    }

    public void setBillID(String billID) {
        BillID = billID;
    }

    public String getTaxID() {
        return TaxID;
    }

    public void setTaxID(String taxID) {
        TaxID = taxID;
    }

    public String getTaxTypeName() {
        return TaxTypeName;
    }

    public void setTaxTypeName(String taxTypeName) {
        TaxTypeName = taxTypeName;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getTaxAmount() {
        return TaxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        TaxAmount = taxAmount;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getTaxSubTypeCode() {
        return TaxSubTypeCode;
    }

    public void setTaxSubTypeCode(String taxSubTypeCode) {
        TaxSubTypeCode = taxSubTypeCode;
    }

    public String getIsSubTax() {
        return IsSubTax;
    }

    public void setIsSubTax(String isSubTax) {
        IsSubTax = isSubTax;
    }
}
