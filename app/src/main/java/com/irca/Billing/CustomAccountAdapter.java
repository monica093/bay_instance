package com.irca.Billing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.fields.Card;
import com.irca.fields.WaiterDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Archanna on 1/28/2017.
 */

public class CustomAccountAdapter extends BaseAdapter {



    ArrayList<Card> masterStore;
    ArrayList<WaiterDetails> waiterDetailses;

    Context context;
    int pageNo;
    public  CustomAccountAdapter(Context c,ArrayList<Card> masterStoreArrayAdapter)
    //ArrayList<WaiterDetails> WaiterDetail
    {
        this.masterStore=masterStoreArrayAdapter;
        //   this.waiterDetailses=WaiterDetail;
        context=c;

    }
    @Override
    public int getCount()
    {
        return masterStore.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_accountlist, parent,false);
        }

        TextView account = (TextView)convertView.findViewById(R.id.account);
        TextView cardName = (TextView)convertView.findViewById(R.id.cardtype);
        TextView steward=(TextView)convertView.findViewById(R.id.steward);
        TextView memberName=(TextView)convertView.findViewById(R.id.MemberName);
        TextView tableNO=(TextView)convertView.findViewById(R.id.tableno);
        TextView otdate=(TextView)convertView.findViewById(R.id.otdate);
        String hifun=masterStore.get(position).getAccount();
        if(hifun.contains("#"))
        {
            String ss[]=masterStore.get(position).getAccount().split("#");
            account.setText(ss[0]);
            cardName.setText(masterStore.get(position).getCardtype());
            steward.setText(masterStore.get(position).getFirstName());
            memberName.setText(masterStore.get(position).getDummy_account());
            tableNO.setText("Table No.  :  " + masterStore.get(position).getTableNO());
            Date dateIn = null;
            try {
             //   dateIn= new SimpleDateFormat("dd-MM-yyyy").parse(masterStore.get(position).getOtdate());
                dateIn= new SimpleDateFormat("yyyy-MM-dd").parse(masterStore.get(position).getOtdate());
                String formatteddateIn = new SimpleDateFormat("dd MMM yyyy").format(dateIn);
                otdate.setText(formatteddateIn);
            } catch (ParseException e) {

            }
       //     otdate.setText( masterStore.get(position).getTableNO());
            //  steward.setText("");

            //   String st[]=waiterDetailses.get(position).getFirstName().toString().split("#");
            //   steward.setText(st[0]);
            //  steward.setText(waiterDetailses.get(position).getFirstName().toString());


        }
        else
        {
            account.setText(masterStore.get(position).getAccount());
            cardName.setText(masterStore.get(position).getCardtype());
            steward.setText(masterStore.get(position).getFirstName());
            steward.setText("");
            steward.setText(waiterDetailses.get(position).getFirstName());
            memberName.setText(masterStore.get(position).getDummy_account());
            tableNO.setText("Table No.  :" +masterStore.get(position).getTableNO());
        }




        //storeName.setText(masterStore.get(position).getAccount());
        return convertView;
    }

}
