package com.irca.cosmo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.irca.fields.Card;
import com.irca.fields.WaiterDetails;

import java.util.ArrayList;

/**
 * Created by Manoch Richard on 25-Apr-16.
 */
public class AccountAdapter  extends BaseAdapter {



        ArrayList<Card> masterStore;
        ArrayList<WaiterDetails> waiterDetailses;

        Context context;
        int pageNo;
       public  AccountAdapter(Context c,ArrayList<Card> masterStoreArrayAdapter)
                //ArrayList<WaiterDetails> WaiterDetail
        {
            this.masterStore=masterStoreArrayAdapter;
         //   this.waiterDetailses=WaiterDetail;
            context=c;

        }
        @Override
        public int getCount()
        {
            return masterStore.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView==null)
            {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.custom_storelist, parent,false);
            }

                TextView storeName = (TextView)convertView.findViewById(R.id.storeText);
                TextView cardName = (TextView)convertView.findViewById(R.id.cardtype);
                TextView steward=(TextView)convertView.findViewById(R.id.steward);
                String hifun=masterStore.get(position).getAccount().toString();


            if(hifun.contains("#"))
                {
                     String ss[]=masterStore.get(position).getAccount().toString().split("#");
                     storeName.setText(ss[0]);
                     cardName.setText(masterStore.get(position).getCardtype());
                     steward.setText(masterStore.get(position).getFirstName().toString());
                   //  steward.setText("");

                 //   String st[]=waiterDetailses.get(position).getFirstName().toString().split("#");
                 //   steward.setText(st[0]);
              //  steward.setText(waiterDetailses.get(position).getFirstName().toString());


                }
                else
                {
                    storeName.setText(masterStore.get(position).getAccount());
                    cardName.setText(masterStore.get(position).getCardtype());
                   steward.setText(masterStore.get(position).getFirstName().toString());
                    steward.setText("");
                    steward.setText(waiterDetailses.get(position).getFirstName().toString());
                }



            //storeName.setText(masterStore.get(position).getAccount());
            return convertView;
        }

}
