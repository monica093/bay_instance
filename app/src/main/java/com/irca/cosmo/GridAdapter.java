package com.irca.cosmo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irca.fields.Table;

import java.util.ArrayList;

/**
 * Created by Manoch Richard on 10/5/2015.
 */
public class GridAdapter extends BaseAdapter {

   //private  int  iconId[];
   private  ArrayList<Table>  name;
    private LayoutInflater inflater;
   public GridAdapter(Context c,ArrayList<Table> mod_text)
    {
        //this.iconId=imagesiD;
        this.name=mod_text;
        inflater= LayoutInflater.from(c);
    }
    @Override
    public int getCount()
    {
      //  return iconId.length;
        return name.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v=convertView;
        try
        {
            v= inflater.inflate(R.layout.dashboard_item_layout,null);
            final ImageView icon= (ImageView) v.findViewById(R.id.imageView);
            final TextView   Modname= (TextView) v.findViewById(R.id.textView);
            if(position%2==0)
            {
                icon.setImageResource(R.drawable.icon_green_64px);
            }
            else
            {
                icon.setImageResource(R.drawable.icon_red_64px);
            }
            //int t=position+1;

                Modname.setText(name.get(position).getTableNmae());


        }
        catch (Exception e)
        {

        }

        return v;
    }
}
