package com.irca.cosmo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.irca.ImageCropping.CircularImage;
import com.irca.MaterialFloatLabel.MaterialAutoCompleteTextView;
import com.irca.MaterialFloatLabel.MaterialEditText;
import com.irca.Printer.Placebill_bakery;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.db.Dbase;
import com.irca.fields.AndroidTempbilldetails;
import com.irca.fields.AndroidTempbilling;
import com.irca.fields.ItemList;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manoch Richard on 03-May-16.
 */
public class Bakery extends Activity {
    LinearLayout itemView;
    TextView itemName,itemCount;
    ImageView add,subtract,search,CLOSE;
    RadioGroup radioGroup;
    RadioButton zero,five;
    LayoutInflater inflater;
    View view = null;
    TextView gName,memberdetails,tableName;
    int noOfCounts=0;
    TextView txt_itemcount;
    Dbase db;
    Button placeOrder;
    ProgressDialog pd;
    float Totalamount=0;
    String memberId="0";
    int tableId=0;
    String creditLimit="0";
    String C="0";
    SharedPreferences sharedpreferences;
    String accountNo="";
    String imei="";
    TelephonyManager tel;
    String cardType="";

    String Nmae;
    String acc;
    public static ArrayList<ItemList> placeOrder_list1;
    String billType="",referenceNo="";
    int tableNo=0;
    ConnectionDetector cd;
    Boolean isInternetPresent=false;


    ImageView find;
    MaterialAutoCompleteTextView searchItemname;
    RadioGroup _rgrp;
    RadioButton _zero,_five;
    View searchView = null;
    LayoutInflater inflater1=null;
    MaterialEditText searchCount;
    Context c;
    AlertDialog builder;
    String groupId;
    ItemnameSearch myadapter;
    String dummy="";
    Boolean selected =false;
    ImageView logo ;
    Bitmap bitmap;
    String serialNo="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kot_itemlist);

     /*   gName= (TextView) findViewById(R.id.group);*/
        txt_itemcount= (TextView) findViewById(R.id.txtCount);
        memberdetails= (TextView) findViewById(R.id.memberDetails);
        tableName= (TextView) findViewById(R.id.tablename);
        placeOrder= (Button) findViewById(R.id.button_placeOrder);
        logo=(ImageView)findViewById(R.id.imageView_logo);
        bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));
        cd = new ConnectionDetector(getApplicationContext());
        find=(ImageView)findViewById(R.id.search);
        isInternetPresent = cd.isConnectingToInternet();
        itemView= (LinearLayout) findViewById(R.id.itemView);
        Bundle b=getIntent().getExtras();
        String groupNmae=b.getString("GroupName");
        Nmae=b.getString("name");
        acc=b.getString("accountNo");
        creditLimit=b.getString("credit");
        groupId=b.getString("groupId");
        tableId=b.getInt("tableId");
        memberId=b.getString("memberId");
        cardType=b.getString("cardType");
        referenceNo=b.getString("referenceNo");
        tableNo=b.getInt("tableNo");

//        gName.setText(groupNmae);
        C=creditLimit;
        tel=(TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
     //   imei=tel.getDeviceId().toString();
        serialNo = Build.SERIAL;
        accountNo=creditLimit;

        tableName.setText("Table" + tableNo);

        txt_itemcount.setText(noOfCounts+"");
        memberdetails.setText("Name:"+Nmae+"\n Id:"+acc+"\nCredit:"+creditLimit+"Rs");
        db=new Dbase(Bakery.this);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        billType=sharedpreferences.getString("billType", "");

        inflater1 = Bakery.this.getLayoutInflater();

        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                itemView = (LinearLayout) findViewById(R.id.itemView);
                itemView.removeAllViews();
                searchView = inflater1.inflate(R.layout.search_bakery, null);
                searchItemname = (MaterialAutoCompleteTextView) searchView.findViewById(R.id.autoCompleteTextView_party);
                searchCount = (MaterialEditText) searchView.findViewById(R.id.eText_qty);
                _rgrp = (RadioGroup) searchView.findViewById(R.id.gr);
                _zero = (RadioButton) searchView.findViewById(R.id.zero);
                _five = (RadioButton) searchView.findViewById(R.id.five);
                myadapter = new ItemnameSearch(Bakery.this, groupId);
                searchItemname.setAdapter(myadapter);
                searchItemname.setThreshold(2);
                CLOSE = (ImageView) searchView.findViewById(R.id.imageView_close11);
                CLOSE.setVisibility(View.GONE);
                alertCustomizedLayout();

                searchItemname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        dummy = myadapter.getItem(position).toString();
                        searchItemname.setEnabled(false);
                        selected = true;
                        CLOSE.setVisibility(View.VISIBLE);
                        searchCount.setFocusable(true);
                        searchCount.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(searchCount, InputMethodManager.SHOW_IMPLICIT);


                    }
                });
            }
        });
        //additems(1);
        db.deleteOT();


        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                isInternetPresent = cd.isConnectingToInternet();

                if(!txt_itemcount.getText().equals("0"))
                {
                    if(billType.equals("BAK"))
                    {

                        if(isInternetPresent){
                            new AsyncPlaceOrder().execute();
                        }else {
                            Toast.makeText(getApplicationContext(),"Internet Connection Lost",Toast.LENGTH_SHORT).show();
                        }

                    }


                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Order any items", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    protected class AsyncPlaceOrder extends AsyncTask<String,Void,String>
    {
        ArrayList<String> credit=null;
        String status="";
        String creditAccountno="";
        String creditName="";
        String storeId = sharedpreferences.getString("storeId", "");
        String loginname=sharedpreferences.getString("loginName","");
        String userId=sharedpreferences.getString("userId","");
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String result="";
        float discount=0;

        @Override
        protected String doInBackground(String... params)
        {
            RestAPI api=new RestAPI(Bakery.this);
            try
            {
                Totalamount=0;
                AndroidTempbilling obj=new AndroidTempbilling();
                //ArrayList<AndroidTempbilldetails> itemList=new ArrayList<>();
                ArrayList<Object> itemList_main=new ArrayList<>();
                placeOrder_list1=new ArrayList<>();
                AndroidTempbilldetails ob=null;
                DecimalFormat formater=new DecimalFormat("#.##");
                ItemList li=null;
                if(itemView!=null)
                {
                    //itemView.bringChildToFront(v);
                    int childRow= itemView.getChildCount();
                    for(int i=0;i<childRow;i++)
                    {
                        View ItemRow=itemView.getChildAt(i);
                        TextView t= (TextView) ItemRow.findViewById(R.id.bak_itemCount);
                        TextView t1= (TextView) ItemRow.findViewById(R.id.bak_itemname);
                        RadioGroup bak_radio= (RadioGroup) ItemRow.findViewById(R.id.myRadioGroup);
                        RadioButton zero= (RadioButton) ItemRow.findViewById(R.id.sound);
                        RadioButton five= (RadioButton) ItemRow.findViewById(R.id.vibration);
                        String Itemcount=t.getText().toString();
                        if(!((Itemcount.equals("")) ||  (Itemcount.equals("0"))))
                        {
                            String itemname=t1.getText().toString();
                            String [] spl=itemname.split("-");
                            String _itemName=spl[1];
                            String _itemCode=spl[0];
                            List<String> itemId=db.getItemid(_itemCode);
                            // String  itemRate=db.getItemRate(_itemCode);

                            ob=new AndroidTempbilldetails();
                            li=new ItemList();

                            ob.setItemId(itemId.get(0));
                            ob.setQuantity(Integer.parseInt(Itemcount));
                            ob.setSalesUintid(i);
                            ob.setRate(itemId.get(1));

                            float salesTax=0;
                            float serviceTax=0;
                            float cessTax=0;

                            int v1=Integer.parseInt(Itemcount);
                            float v2=Float.parseFloat(itemId.get(1));
                            float  v3=v1 * v2;  //for calculation of discount
                            float v4=v1*v2;  //for total amount without discount
                            int selectedId = bak_radio.getCheckedRadioButtonId();

                            if(selectedId == five.getId())
                            {
                                discount=(float)(v3 * 5.5)/100;
                                v3=(float)(v4-((v3 * 5.5)/100));
                                ob.setAmount(v3);
                            }
                            else
                            {
                                discount=0;
                                ob.setAmount(v3);
                            }


                            //salesTax=(v3*Float.parseFloat(itemId.get(2)))/100;
                            //serviceTax=(v3*Float.parseFloat(itemId.get(4)))/100;
                            //cessTax=(v3*Float.parseFloat(itemId.get(3)))/100;
                            //ob.setSalestax(salesTax);
                            //ob.setServicetax(serviceTax);
                            //ob.setCesstax(cessTax);
                            if(!cardType.equals("CLUB CARD"))
                            {
                                salesTax=(v3*Float.parseFloat(itemId.get(2)))/100;
                                serviceTax=(v3*Float.parseFloat(itemId.get(4)))/100;
                                cessTax=(v3*Float.parseFloat(itemId.get(3)))/100;
                                ob.setSalestax(Float.valueOf(formater.format(salesTax)));
                                ob.setServicetax(Float.valueOf(formater.format(serviceTax)));
                                ob.setCesstax(Float.valueOf(formater.format(cessTax)));

                                li.setTax(Float.toString(serviceTax));
                                li.setVat(Float.toString(salesTax));
                                li.setCesstax(Float.toString(cessTax));
                            }
                            else
                            {
                                ob.setSalestax(0);
                                ob.setServicetax(0);
                                ob.setCesstax(0);

                                li.setTax(Float.toString(0));
                                li.setVat(Float.toString(0));
                                li.setCesstax(Float.toString(0));
                            }
                            Totalamount=Totalamount+ob.getAmount()+ob.getServicetax()+ob.getCesstax()+ob.getSalestax();


                            li.setItemCode(_itemCode);
                            li.setItemName(_itemName);
                            li.setQuantity(Itemcount);
                            li.setAmount(Float.toString(v3));  //with discounted value
                            li.setCardType(cardType);
                            li.setRate(itemId.get(1));
                            li.setBillAmount(Float.toString(v4));   // without discount
                            li.setDiscount(Float.toString(discount));


                            itemList_main.add(ob);
                            placeOrder_list1.add(li);

                            obj.setMemberId(memberId);
                            obj.setStoreId(storeId);
                            obj.setTableId(tableId + "");
                            // obj.setTamount(Float.parseFloat(formater.format(Totalamount)));
                            float dummy =Float.valueOf(formater.format(Totalamount));
                            obj.setTamount(Float.valueOf(formater.format(Totalamount)));
                        }

                    }
                }

                if(itemList_main.size()!=0)
                {

                    if(!cardType.equals("CASH CARD")){

                        if(C.equals("-0")) {
                            JSONObject jsonObject=api.cms_placeOrder3(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo);
                            status=jsonObject.toString();
                            result=jsonObject.optString("Value");
                        }

                        else if(Totalamount<=(25000-Float.parseFloat(C))) {
                            JSONObject jsonObject=api.cms_placeOrder3(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo);
                            status=jsonObject.toString();
                            result=jsonObject.optString("Value");
                        }
                        else

                        {
                            status="Credit exceeds";
                        }

                    }else{

                        if(Float.parseFloat(C)-Totalamount>0){

                            JSONObject jsonObject= api.cms_placeOrder3(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo);
                            status=jsonObject.toString();
                            result=jsonObject.optString("Value");

                        }else{
                            status="Credit exceeds";
                        }

                    }
                }
                else
                {
                    status="No item";
                }

            } catch (Exception e)
            {
                e.printStackTrace();
                status=e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pd=new ProgressDialog(Bakery.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            pd.dismiss();
            if(s.equals("Credit exceeds"))
            {
                if(cardType.equals("CASH CARD")){

                    showMsg(Totalamount,C);
                }else{
                    Toast.makeText(getApplicationContext(),"Credit limit exceeds", Toast.LENGTH_LONG).show();
                }

            }
            else if(s.contains("true"))
            {
                db.deleteAccountInbakery(memberId);
                Toast.makeText(getApplicationContext(),"Order placed", Toast.LENGTH_LONG).show();
                Intent i=new Intent(Bakery.this, Placebill_bakery.class);
                i.putExtra("mName",Nmae);
                i.putExtra("a/c", acc);
                i.putExtra("tableNo",tableNo);
                i.putExtra("tableId",tableId);
                i.putExtra("bill_id",result);
                i.putExtra("Type","k");
                startActivity(i);
                finish();
            }
            else if(s.contains("No item"))
            {
                Toast.makeText(getApplicationContext(),"No item is ordered-->"+s, Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Order not placed-->"+s, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showMsg(float totalAmount,String creditLimit) {
        AlertDialog.Builder builder= new AlertDialog.Builder(c);
        builder.setMessage("Credit Limit Exceeds" +"\n Total Amount : "+totalAmount +"\n Credit Limit : "+ creditLimit);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.create();
        builder.show();


    }

    public void additems(int value)
    {
        ArrayList<String> list=null;
        noOfCounts=0;
        if(value==1)
        {
            list = db.getItemList(groupId);
        }
        else
        {
            // itemView.removeAllViews();
            itemView=null;
            view=null;
            itemName=null;
            itemCount=null;
            add=null;
            subtract=null;
            list = db.getOTItemList_bak();
        }

        for (int i = 0; i < list.size(); i++)
        {
            if (itemView == null) {
                if(itemView==null)
                {
                    itemView= (LinearLayout) findViewById(R.id.itemView);
                    view = getLayoutInflater().inflate(R.layout.bakery_list, itemView,false);
                    //itemlist= (LinearLayout)view.findViewById(R.id.kotItemlist);
                    itemName= (TextView)view. findViewById(R.id.bak_itemname);
                    itemCount= (TextView)view.findViewById(R.id.bak_itemCount);
                    add=(ImageView) view.findViewById(R.id.bak_add);
                    subtract=(ImageView) view.findViewById(R.id.bak_minus);
                    radioGroup= (RadioGroup) view.findViewById(R.id.myRadioGroup);
                    zero= (RadioButton) view.findViewById(R.id.sound);
                    five= (RadioButton) view.findViewById(R.id.vibration);
                }
                else
                {
                    view = getLayoutInflater().inflate(R.layout.bakery_list, itemView,false);
                    // itemlist= (LinearLayout)view.findViewById(R.id.kotItemlist);
                    itemName= (TextView)view. findViewById(R.id.bak_itemname);
                    itemCount= (TextView)view.findViewById(R.id.bak_itemCount);
                    add=(ImageView) view.findViewById(R.id.bak_add);
                    subtract=(ImageView) view.findViewById(R.id.bak_minus);
                    itemView= (LinearLayout) findViewById(R.id.itemView);
                    radioGroup= (RadioGroup) view.findViewById(R.id.myRadioGroup);
                    zero= (RadioButton) view.findViewById(R.id.sound);
                    five= (RadioButton) view.findViewById(R.id.vibration);
                }
            }
            else {
                view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                view = getLayoutInflater().inflate(R.layout.bakery_list, itemView,false);
                //itemlist= (LinearLayout)view.findViewById(R.id.kotItemlist);
                itemName= (TextView)view. findViewById(R.id.bak_itemname);
                itemCount= (TextView)view.findViewById(R.id.bak_itemCount);
                add=(ImageView) view.findViewById(R.id.bak_add);
                subtract=(ImageView) view.findViewById(R.id.bak_minus);
                //radioGroup= (RadioGroup) view.findViewById(R.id.myRadioGroup);
                radioGroup= (RadioGroup) view.findViewById(R.id.myRadioGroup);
                zero= (RadioButton) view.findViewById(R.id.sound);
                five= (RadioButton) view.findViewById(R.id.vibration);
            }
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id=v.getId();

                    ViewGroup parent = (ViewGroup) v.getParent();
                    TextView t1= (TextView) parent.findViewById(R.id.bak_itemCount);
                    TextView t2= (TextView) parent.findViewById(R.id.bak_itemname);
                    RadioGroup gr=(RadioGroup)parent.findViewById(R.id.myRadioGroup);
                    RadioButton zero = (RadioButton) parent.findViewById(R.id.sound);
                    RadioButton  five = (RadioButton) parent.findViewById(R.id.vibration);

                    int checked=gr.getCheckedRadioButtonId();
                    String _itemname = t2.getText().toString();
                    String _itemcode = t1.getText().toString();

                    String count= t1.getText().toString();
                    if(count.equals(""))
                    {

                        t1.setText("1");
                        noOfCounts=noOfCounts+1;

                        if(_itemname.contains("-")){
                            String array[] = _itemname.split("-");
                            if(checked==five.getId()){
                                long   rvalur = db.insertOT_bak(array[0], array[1], "1","1");
                            }else{
                                long  rvalur = db.insertOT_bak(array[0], array[1], "1", "0");
                            }

                        }else{

                        }


                        // txt_itemcount.setText(noOfCounts+"");
                    }
                    else
                    {
                        int c=Integer.parseInt(count);
                        t1.setText(c+1+"");
                        noOfCounts=noOfCounts+1;

                        if(_itemname.contains("-"))
                        {
                            String array[] = _itemname.split("-");
                            try{
                                if(checked==five.getId()){
                                    long   rvalur = db.insertOT_bak(array[0], array[1], Integer.toString(c+1),"1");
                                }else{
                                    long  rvalur = db.insertOT_bak(array[0], array[1],  Integer.toString(c+1), "0");
                                }
                            }catch(Exception e){
                                String r=e.getMessage().toString();
                            }



                        }else{

                        }
                        //  txt_itemcount.setText(noOfCounts+"");
                    }


                }
            });
            subtract.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    try
                    {
                        ViewGroup parent = (ViewGroup) v.getParent();
                        TextView t1= (TextView) parent.findViewById(R.id.bak_itemCount);
                        TextView t2= (TextView) parent.findViewById(R.id.bak_itemname);
                        RadioGroup gr=(RadioGroup)parent.findViewById(R.id.myRadioGroup);
                        RadioButton zero = (RadioButton) parent.findViewById(R.id.sound);
                        RadioButton  five = (RadioButton) parent.findViewById(R.id.vibration);


                        String count= t1.getText().toString();

                        int checked=gr.getCheckedRadioButtonId();
                        String _itemname = t2.getText().toString();
                        String _itemcode = t1.getText().toString();


                        if(count.equals(""))
                        {

                            Toast.makeText(getApplicationContext(), "Add item first", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            int c=Integer.parseInt(count);
                            if(c!=0)
                            {
                                t1.setText(c-1+"");
                                noOfCounts=noOfCounts-1;

                                if(_itemname.contains("-"))
                                {
                                    String array[] = _itemname.split("-");
                                    if(checked==five.getId()){
                                        long   rvalur = db.insertOT_bak(array[0], array[1], Integer.toString(c-1),"1");
                                    }else{
                                        long  rvalur = db.insertOT_bak(array[0], array[1],  Integer.toString(c-1), "0");
                                    }

                                }else{

                                }
                                // txt_itemcount.setText(noOfCounts+"");
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        String hh=e.getMessage().toString();
                    }
                }
            });
            //itemName.setId(i);
            // itemCount.setId(i);
            //add.setId(i);
            //subtract.setId(i);
            String ll[] = list.get(i).toString().split("\\$");
            if(ll.length>1)
            {
                itemName.setText(ll[0]);
                itemCount.setText(ll[1]);
                noOfCounts=noOfCounts+Integer.parseInt(ll[1]);
                txt_itemcount.setText(list.size()+"");
                if(ll[2].equals("1")){
                    zero.setChecked(false);
                    five.setChecked(true);
                }else{
                    zero.setChecked(true);
                    five.setChecked(false);
                }
            } else
            {
                itemName.setText(ll[0]);
                zero.setChecked(true);
                five.setChecked(false);
            }
                /*if(ll.length()==1)
                {
                    try
                    {
                        alpahabet=new TextView(KotItemlist.this);
                        alpahabet.setText(ll);
                        View  A=alpahabet;
                        itemView.addView(A);
                    }
                    catch (Exception e)
                    {
                        String mm=e.getMessage().toString();
                    }
                }
                else
                {
                    itemName.setText(ll);
                    itemView.addView(view);
                }*/
            itemView.addView(view);
        }
        //itemView.setVisibility(View.VISIBLE);
    }

    public void alertCustomizedLayout()
    {

        builder = new AlertDialog.Builder(Bakery.this).setView(searchView)
                .setTitle("")
                .setNegativeButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        additems(2);
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }

                })
                .show();

        builder.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                Long rvalur;
                String itemName = searchItemname.getText().toString();
                String count = searchCount.getText().toString();
                int checked=_rgrp.getCheckedRadioButtonId();
                if (!(itemName.equals("") || count.equals("") || count.equals("0"))) {
                    String array[] = itemName.split("_");
                    if(checked==_five.getId()){
                        rvalur = db.insertOT_bak(array[1], array[0], count,"1");
                    }else{
                        rvalur = db.insertOT_bak(array[1], array[0], count, "0");
                    }

                    if (rvalur < 0) {
                        Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                    } else{
                        Toast.makeText(getApplicationContext(), "Item  added", Toast.LENGTH_SHORT).show();
                        searchItemname.setText("");
                        searchCount.setText("");
                        searchItemname.setFocusable(true);
                        searchItemname.requestFocus();
                        _zero.setChecked(true);
                        _five.setChecked(false);
                        searchItemname.setEnabled(true);
                        CLOSE.setVisibility(View.GONE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(searchItemname, InputMethodManager.SHOW_IMPLICIT);

                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Enter Valid item Name and quantity", Toast.LENGTH_LONG).show();
                }

                /*additems(2);
                builder.cancel();*/
            }
        });

        builder.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                int count=db.getItemList1(groupId);
                if(count==0){
                    additems(1);
                    builder.cancel();
                }else{
                    additems(2);
                    builder.cancel();
                }
            }
        });
//        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface dialog) {
//                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
//                Button c = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
//                b.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View view) {
//                        // TODO Do something
//
//                        //Dismiss once everything is OK.
//                        additems(2);
//                        alertDialog.dismiss();
//                    }
//                });
//                c.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View view) {
//                        // TODO Do something
//
//                        //Dismiss once everything is OK.
//                        String itemName = searchItemname.getText().toString();
//                        String count = searchCount.getText().toString();
//                        if (!(itemName.equals("") || count.equals("") || count.equals("0"))) {
//                            String array[] = itemName.split("-");
//                            Long rvalur = db.insertOT(array[0], array[1], count);
//                            if (rvalur < 0) {
//                                Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
//                            }
//                        } else {
//                            Toast.makeText(getApplicationContext(), "Dont give empty values", Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                });
//            }
//        });

//                // action buttons
//                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        // your sign in code here
//                        additems(2);
//                        dialog.dismiss();
//                    }
//                })
//                .setNegativeButton("Save", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        // remove the dialog from the screen
//                        String itemName = searchItemname.getText().toString();
//                        String count = searchCount.getText().toString();
//                        if (!(itemName.equals("") || count.equals("") || count.equals("0"))) {
//                            String array[] = itemName.split("-");
//                            Long rvalur = db.insertOT(array[0], array[1], count);
//                            if (rvalur<0)
//                            {
//                                Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
//                            }
//                        } else {
//                            Toast.makeText(getApplicationContext(), "Dont give empty values", Toast.LENGTH_LONG).show();
//                        }
//
//
//                    }
//                })
        // .show();

    }

    public void onclick(View v)
    {
        searchItemname.setText("");
        CLOSE.setVisibility(View.GONE);
        searchItemname.setEnabled(true);
    }
    protected class AsyncSearch extends AsyncTask<String, Void, String> {
        String dis="";

        @Override
        protected String doInBackground(String... params) {

            try {

                if (itemView != null)
                {
                    int childRow = itemView.getChildCount();
                    for (int i = 0; i < childRow; i++)
                    {
                        View ItemRow = itemView.getChildAt(i);
                        TextView t = (TextView) ItemRow.findViewById(R.id.bak_itemCount);
                        TextView t1 = (TextView) ItemRow.findViewById(R.id.bak_itemname);
                        RadioGroup bak_radio= (RadioGroup) ItemRow.findViewById(R.id.myRadioGroup);
                        RadioButton zero= (RadioButton) ItemRow.findViewById(R.id.sound);
                        RadioButton five= (RadioButton) ItemRow.findViewById(R.id.vibration);
                        String Itemcount = t.getText().toString();
                        if (!((Itemcount.equals("")) || (Itemcount.equals("0"))))
                        {
                            String itemname = t1.getText().toString();
                            String[] spl = itemname.split("-");
                            String _itemCode = spl[0];

                            int selectedId = bak_radio.getCheckedRadioButtonId();

                            if(selectedId == five.getId())
                            {
                                dis="1";
                            }
                            else
                            {
                                dis="0";
                            }
                            db.insertOT_bak(_itemCode, spl[1], Itemcount,dis);
                        }
                    }
                }



            } catch (Exception e) {
                e.printStackTrace();

            }
            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Bakery.this);
            pd.show();
            pd.setMessage("Searching please wait ....");
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            itemView.removeAllViews();
            searchView = inflater1.inflate(R.layout.search_bakery, null);
            searchItemname = (MaterialAutoCompleteTextView) searchView.findViewById(R.id.autoCompleteTextView_party);
            searchCount= (MaterialEditText) searchView.findViewById(R.id.eText_qty);
            _rgrp=(RadioGroup)searchView.findViewById(R.id.gr);
            _zero=(RadioButton)searchView.findViewById(R.id.zero);
            _five=(RadioButton)searchView.findViewById(R.id.five);
            myadapter=new ItemnameSearch(Bakery.this, groupId);
            searchItemname.setAdapter(myadapter);
            searchItemname.setThreshold(2);
            search= (ImageView) searchView.findViewById(R.id.imageView_close1);
            search.setVisibility(View.GONE);
            alertCustomizedLayout();

            searchItemname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    dummy = myadapter.getItem(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            searchItemname.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (dummy.equals("")) {
                        searchItemname.setEnabled(false);
                        search.setVisibility(View.VISIBLE);
                    } else {
                        searchItemname.setEnabled(true);
                        search.setVisibility(View.GONE);
                    }
                }
            });
            pd.dismiss();

        }
    }

}
