package com.irca.cosmo;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.irca.Billing.Activity_Signature;
import com.irca.Billing.BillingProfile;
import com.irca.Billing.Dashboard_NSCI;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

public class BillPrintPDFActivity extends AppCompatActivity {
    WebView webview;
    TextView tv_header;
    Bundle bundle;
    String billid = "",memberid = ""
            ,memName = "",memAccNo = ""
            ,waiternamecode = "",_isdebitcard = ""
            ,cardType = "",signimg = "",Billdetailno = "" ;
    int page=0,tableNo=0,tableId=0;
    SharedPreferences sharedpreferences;    String url="";

    // for PDF view.
    PDFView pdfView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            setContentView(R.layout.activity_bill_print_pdfactivity);
            tv_header=findViewById(R.id.tv_header);
            sharedpreferences =  getSharedPreferences("URLS", Context.MODE_PRIVATE);
            url  = sharedpreferences.getString("url", "");
            bundle = getIntent().getExtras();
            if (bundle != null) {
                page = bundle.getInt("pageValue");
                if(page==0){
                    tv_header.setText("Capture Signature");
                    billid = bundle.getString("billid");
                    memberid = bundle.getString("accno");
                    memName = bundle.getString("creditName");
                    memAccNo = bundle.getString("creditAno");
                    tableNo = bundle.getInt("tableNo");
                    tableId = bundle.getInt("tableId");
                    waiternamecode = bundle.getString("waiternamecode");
                    _isdebitcard = bundle.getString("isdebitcard");
                    cardType = bundle.getString("cardType");//CASH CARD
                    signimg = bundle.getString("signimg");//CASH CARD
                    Billdetailno = bundle.getString("billno");//CASH CARD
                }
                else {
                    tv_header.setText("Close");
                    Billdetailno = bundle.getString("billno");//CASH CARD
                }

            }

            tv_header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(tv_header.getText().equals("Capture Signature")){
                        Intent intent = new Intent(BillPrintPDFActivity.this, Activity_Signature.class);
                        intent.putExtra("billid", billid);
                        intent.putExtra("accno",memberid);
                        intent.putExtra("pageValue", 0);
                        intent.putExtra("isdebitcard", _isdebitcard);
                        intent.putExtra("creditAno", memAccNo);
                        intent.putExtra("creditName", memName);
                        intent.putExtra("tableNo", tableNo);
                        intent.putExtra("tableId", tableId);
                        //  i.putExtra("waiterid", waiterid);
                        intent.putExtra("cardType", cardType);
                        intent.putExtra("waiternamecode", waiternamecode);
                        intent.putExtra("billno", Billdetailno);
                        startActivity(intent);
                    }
                    else {
                        Intent intent = new Intent(BillPrintPDFActivity.this, Dashboard_NSCI.class);
                        startActivity(intent);
                        finish();
                    }

                }
            });
     /*   web =(WebView)findViewById(R.id.pdfView);

        String url = "http://docs.google.com/gview?embedded=true&url=" + "http://122.166.147.16//mobile_tab_billing_bay/IRCA%20POS%20BILL%20PDF/2022/MARCH/13/202110000450.pdf";
        String doc="<iframe src='"+url+"' width='100%' height='100%' style='border: none;'></iframe>";

        web.getSettings().setJavaScriptEnabled(true);
        web.loadData(doc, "text/html", "UTF-8");
*/
            Calendar cal=Calendar.getInstance();
            SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
            String month_name = month_date.format(cal.getTime());
            Log.d("Current month_name => " , month_name.toString());

            Date c = Calendar.getInstance().getTime();
            Log.d("Current time => " , c.toString());

            SimpleDateFormat df = new SimpleDateFormat("dd", Locale.getDefault());
            String formattedDate = df.format(c);
            Log.d("Current date => " , formattedDate.toString());

            String ip=url.replace("handler1.ashx","");

      this.webview = (WebView) findViewById(R.id.pdfView);

        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        final Dialog progressBar = ProgressDialog.show(BillPrintPDFActivity.this, "Bill View", "Loading...");

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
              //  Log.i(TAG, "Processing webview url click...");
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
           //     Log.i(TAG, "Finished loading URL: " + url);
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }


        });

     //   String url = "http://docs.google.com/gview?embedded=true&url=" + "http://122.166.147.16//mobile_tab_billing_bay/IRCA%20POS%20BILL%20PDF/2022/"+month_name+"/"+formattedDate+"/"+Billdetailno+".pdf";
        String url = "http://docs.google.com/gview?embedded=true&url=" + ip+"/IRCA%20POS%20BILL%20PDF/2022/"+month_name+"/"+formattedDate+"/"+Billdetailno+".pdf";
  Log.d("url to load: ",url);
        String doc="<iframe src='"+url+"' width='100%' height='100%' style='border: none;'></iframe>";

        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadData(doc, "text/html", "UTF-8");



            //webview.loadUrl("http://www.google.com");

                 }
        // create an async task class for loading pdf file from URL.


        catch (Exception e){
Log.d("Exception",e+"");
        }

    }



     }


