package com.irca.cosmo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.irca.fields.Taccount;

import java.util.ArrayList;

/**
 * Created by Manoch Richard on 29-Apr-16.
 */
public class Tlistadapter extends BaseAdapter {

    ArrayList<Taccount> masterStore;
    Context context;
    String pageNo="";
    Tlistadapter(Context c,ArrayList<Taccount> masterStoreArrayAdapter,String mPage)
    {
        this.masterStore=masterStoreArrayAdapter;
        context=c;
        pageNo=mPage;
    }
    @Override
    public int getCount()
    {

        return masterStore.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_storelist, parent,false);
        }
        TextView storeName = (TextView)convertView.findViewById(R.id.storeText);
        TextView cardName = (TextView)convertView.findViewById(R.id.cardtype);


        if(!masterStore.get(position).getBillNo().equals("") || masterStore.get(position).getBillNo().length()>4)
        {
            storeName.setText(masterStore.get(position).getAcc() + "   -   "+masterStore.get(position).getBillNo());
        }
        else
        {
            storeName.setText(masterStore.get(position).getAcc() + "   -   "+masterStore.get(position).getBillNo());
        }
       // if(pageNo.equals("0")){
          //  storeName.setText(masterStore.get(position).getAcc() + "   -   "+masterStore.get(position).getBillNo().substring(4));
      /*  }else{
            storeName.setText(masterStore.get(position).getAcc() + "   -   "+masterStore.get(position).getBillNo().substring(4));
        }*/

       // cardName.setText(masterStore.get(position).);
        return convertView;
    }
}
