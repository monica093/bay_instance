package com.irca.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.epson.epos2.printer.Printer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.Printer.DiscoveryActivity;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.adapter.DiscountItemAdapter;
import com.irca.adapter.ProvisionalBillAdapter;
import com.irca.adapter.RunningOrdersAdapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.dto.ProvisionalDto1;
import com.irca.dto.ProvisionalDto2;
import com.irca.dto.TableTransferDetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DiscountActivity extends AppCompatActivity {

    ConnectionDetector cd;
    boolean isInternetPresent;
    RecyclerView recyclerView;

    SharedPreferences sharedpreferences, pref;
    String posId, userId;
    ImageView backImageView;
    Button button, button1;
    DiscountItemAdapter orderListAdapter;
    TextView mName, disAmt, disPer, tamt, g_tot, g_sgst, g_vat, g_cgst;
    DecimalFormat format = new DecimalFormat("#.##");

    String billId;
    Double billAmt = 0.0;
    String method = "";

    List<ProvisionalDto2> itemsList1 = new ArrayList<>();
    List<ProvisionalDto1> mainList1 = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount);
        try {
            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            mName = findViewById(R.id.mName);
            disPer = findViewById(R.id.et_disPer);
            disAmt = findViewById(R.id.et_disAmt);
            tamt = findViewById(R.id.tamt);
            g_tot = findViewById(R.id.g_tot);
            g_sgst = findViewById(R.id.sgst);
            g_cgst = findViewById(R.id.cgst);
            g_vat = findViewById(R.id.vat);
            recyclerView = findViewById(R.id.recycler_view);
            backImageView = findViewById(R.id.image_viewBack);
            button = findViewById(R.id.button);
            button1 = findViewById(R.id.button1);

            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            pref = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
            posId = sharedpreferences.getString("storeId", "");
            userId = sharedpreferences.getString("userId", "");
            billId = getIntent().getStringExtra("billId");
            backImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });


            new CallApi().execute();

            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!disPer.getText().toString().trim().isEmpty()) {
                        calculatePer(Integer.parseInt(disPer.getText().toString().trim()));
                    }
                }
            });


        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    private void calculatePer(int per) {

    }

    @SuppressLint("StaticFieldLeak")
    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(DiscountActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<ProvisionalDto2> itemsList = new ArrayList<>();
        List<ProvisionalDto1> mainList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(DiscountActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.GetProvisionalBill("15072");
                    result = "" + getResult;
                    if (result.contains("true")) {
                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = jsonObject.getJSONArray("Table");
                        JSONArray jsonArray1 = jsonObject.getJSONArray("Table1");
                        try {
                            Gson gson = new Gson();
                            mainList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<ProvisionalDto1>>() {
                            }.getType());
                            itemsList = gson.fromJson(String.valueOf(jsonArray1), new TypeToken<List<ProvisionalDto2>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(DiscountActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        try {
                            pd.dismiss();
                            if (mainList.size() > 0) {
                                mainList1.clear();
                                mainList1.addAll(mainList);
                                mName.setText(mainList.get(0).getAccountNumber() + "-" + mainList.get(0).getMemberName());
                            } else {
                                Toast.makeText(DiscountActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                            }
                            if (itemsList.size() > 0) {
                                itemsList1.clear();
                                itemsList1.addAll(itemsList);
                                showData();
//                                Double amt = 0.0, cgst = 0.0, sgst = 0.0, vat = 0.0, total = 0.0;
//                                for (int i = 0; i < itemsList.size(); i++) {
//                                    Double d = Double.parseDouble(itemsList.get(i).getTRate()) * Double.parseDouble(itemsList.get(i).getQuantity());
//                                    amt = amt + d;
//                                    Double cgst1 = Double.parseDouble(itemsList.get(i).getCgst());
//                                    cgst = cgst + cgst1;
//                                    Double sgst1 = Double.parseDouble(itemsList.get(i).getSgst());
//                                    sgst = sgst + sgst1;
//                                    Double vat1 = Double.parseDouble(itemsList.get(i).getVatAmount());
//                                    vat = vat + vat1;
//                                    total = total + (d + cgst1 + sgst1 + vat1);
//                                }
//                                DecimalFormat precision = new DecimalFormat("0.00");
//                                tamt.setText(precision.format(amt) + "");
//                                g_tot.setText(precision.format(total) + "");
//                                g_vat.setText(precision.format(vat) + "");
//                                g_cgst.setText(precision.format(cgst) + "");
//                                g_sgst.setText(precision.format(sgst) + "");
                            } else {
                                Toast.makeText(DiscountActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                        break;
                    case "server":
                        Toast.makeText(DiscountActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(DiscountActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(DiscountActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(DiscountActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }

        public void showData() {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DiscountActivity.this);
            recyclerView.setLayoutManager(linearLayoutManager);
            orderListAdapter = new DiscountItemAdapter(itemsList1, DiscountActivity.this);
            recyclerView.setAdapter(orderListAdapter);
            orderListAdapter.notifyDataSetChanged();
        }


    }

}
