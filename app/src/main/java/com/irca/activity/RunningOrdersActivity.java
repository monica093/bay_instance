package com.irca.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.adapter.RunningOrdersAdapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.dto.TableTransferDetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RunningOrdersActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ConnectionDetector cd;
    boolean isInternetPresent;
    SharedPreferences sharedpreferences;
    String posId, userId;
    ImageView backImageView;
    EditText searchEditText;
    List<TableTransferDetails> tableTransferDetailsList = new ArrayList<>();
    RunningOrdersAdapter orderListAdapter;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running_orders);
        try {
            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            recyclerView = findViewById(R.id.recycler_view);
            searchEditText = findViewById(R.id.edit_textSearch);
            backImageView = findViewById(R.id.image_viewBack);


            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            posId = sharedpreferences.getString("storeId", "");
            userId = sharedpreferences.getString("userId", "");

            backImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            if (posId.isEmpty()) {
                Toast.makeText(this, "Please select POS", Toast.LENGTH_SHORT).show();
                finish();
            } else if (userId.isEmpty()) {
                Toast.makeText(this, "Authentication failed, Please login again", Toast.LENGTH_SHORT).show();
            } else {
                new CallApi().execute();
            }
            searchEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    filter(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void filter(String text) {
        try {
            List<TableTransferDetails> itemsArrayList = new ArrayList<>();
            for (int i = 0; i < tableTransferDetailsList.size(); i++) {
                if (tableTransferDetailsList.get(i).getOTNo().toLowerCase().contains(text.toLowerCase()) || tableTransferDetailsList.get(i).getTableNumber().toLowerCase().contains(text.toLowerCase())) {
                    itemsArrayList.add(tableTransferDetailsList.get(i));
                }
            }
            orderListAdapter.filterList(itemsArrayList, this,userId);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("StaticFieldLeak")
    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(RunningOrdersActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<TableTransferDetails> itemsList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(RunningOrdersActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.CmsGetRunningOrders(posId, userId);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = jsonObject.getJSONArray("Table");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<TableTransferDetails>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(RunningOrdersActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        if (itemsList.size() > 0) {
                            tableTransferDetailsList = itemsList;
                            showData(itemsList);
                        } else {
                            Toast.makeText(RunningOrdersActivity.this, "Running Orders not found", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(RunningOrdersActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(RunningOrdersActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(RunningOrdersActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(RunningOrdersActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }

    }

    public void showData(List<TableTransferDetails> itemsList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RunningOrdersActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        orderListAdapter = new RunningOrdersAdapter(itemsList, RunningOrdersActivity.this,userId);
        recyclerView.setAdapter(orderListAdapter);
        orderListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        finish();
    }

}
