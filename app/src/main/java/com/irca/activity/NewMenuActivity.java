package com.irca.activity;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.irca.Billing.Dashboard_NSCI;
import com.irca.cosmo.R;

import java.util.Objects;

public class NewMenuActivity extends AppCompatActivity {


    LinearLayout qLinearLayout, mLinearLayout;
    ImageView backImageView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_menu);
        try {
            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }

            qLinearLayout = findViewById(R.id.linearLayoutQ);
            mLinearLayout = findViewById(R.id.linearLayoutM);
            backImageView = findViewById(R.id.image_viewBack);

            mLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(NewMenuActivity.this, MenuNewListActivity.class));
                }
            });
            qLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(NewMenuActivity.this, QRCodeActivity.class));
                }
            });
            backImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(NewMenuActivity.this, Dashboard_NSCI.class));
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(NewMenuActivity.this, Dashboard_NSCI.class));
    }

}
