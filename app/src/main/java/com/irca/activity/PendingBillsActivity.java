package com.irca.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.adapter.PendingBillsAdapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.dto.PendingBillsDto;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class PendingBillsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ConnectionDetector cd;
    boolean isInternetPresent;
    SharedPreferences sharedpreferences;
    String posId, userId;
    EditText fromdatetxt, todatetxt, idEditText;
    ImageView backImageView;
    Button submitButton;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_bills);
        try {
            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            recyclerView = findViewById(R.id.recycler_view);
            backImageView = findViewById(R.id.image_viewBack);
            idEditText = findViewById(R.id.edit_textId);
            submitButton = findViewById(R.id.submit);
            fromdatetxt = findViewById(R.id.frmdatetxt);
            todatetxt = findViewById(R.id.todatetxt);

            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            posId = sharedpreferences.getString("storeId", "");
            userId = sharedpreferences.getString("userId", "");
            if (posId.isEmpty()) {
                Toast.makeText(this, "Please select POS", Toast.LENGTH_SHORT).show();
                finish();
            }

            backImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (fromdatetxt.getText().toString().trim().isEmpty()) {
                        Toast.makeText(PendingBillsActivity.this, "Please select from date", Toast.LENGTH_SHORT).show();
                    } else if (fromdatetxt.getText().toString().trim().isEmpty()) {
                        Toast.makeText(PendingBillsActivity.this, "Please select to date", Toast.LENGTH_SHORT).show();
                    } else {
                        if (userId.equals("1")) {
                            new CallApi().execute(idEditText.getText().toString().trim(), fromdatetxt.getText().toString().trim(), todatetxt.getText().toString().trim(), posId, "");
                        } else {
                            new CallApi().execute(idEditText.getText().toString().trim(), fromdatetxt.getText().toString().trim(), todatetxt.getText().toString().trim(), posId, userId);
                        }
                    }
                }
            });
            fromdatetxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                DialogFragment dialogfragment = new DatePickerDialogThemeIN();
//
//                dialogfragment.show(getFragmentManager(), "Theme 2");

                    showFromDatepicker();

                }
            });
            todatetxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDatepicker();

//                DialogFragment dialogfragmentOut = new DatePickerDialogThemeOut();
//
//                dialogfragmentOut.show(getFragmentManager(), "Theme 2");
                }
            });


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void showDatepicker() {

        DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int j, int k) {
                j = j + 1;
                Calendar c2 = Calendar.getInstance();
                c2.set(i, j - 1, k);
                Calendar c = Calendar.getInstance();
                Date time = c2.getTime();
                Date time2 = c.getTime();
                if (c2.getTime().after(time2)) {
                    Toast.makeText(PendingBillsActivity.this, "Please choose valid date", Toast.LENGTH_SHORT).show();
                } else {
                    todatetxt.setText(getFormatedDate(i + "-" + j + "-" + k));
                }
            }
        };

        String[] date_split = getTodysdate();
        DatePickerDialog datePickerDialog =
                //new DatePickerDialog(Billing.this,R.style.MyDialogTheme,myDateSetListener, 2017, 9, 13);
                new DatePickerDialog(PendingBillsActivity.this, myDateSetListener, Integer.parseInt(date_split[2]), Integer.parseInt(date_split[1]) - 1, Integer.parseInt(date_split[0]));
        datePickerDialog.setTitle("To Date");
        datePickerDialog.show();
    }

    private String getFormatedDate(String d1) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date d2 = null;
        try {
            d2 = df.parse(d1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return df.format(d2);
    }

    private String[] getTodysdate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate.split("-");
    }

    private void showFromDatepicker() {
        DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int j, int k) {
                j = j + 1;
                Calendar c2 = Calendar.getInstance();
                c2.set(i, j - 1, k);
                Calendar c = Calendar.getInstance();
                Date time = c2.getTime();
                Date time2 = c.getTime();
                if (c2.getTime().after(time2)) {
                    Toast.makeText(PendingBillsActivity.this, "Please choose valid date", Toast.LENGTH_SHORT).show();
                } else {
                    fromdatetxt.setText(getFormatedDate(i + "-" + j + "-" + k));
                }
            }
        };

        String[] date_split = getTodysdate();
        DatePickerDialog datePickerDialog =
                //new DatePickerDialog(Billing.this,R.style.MyDialogTheme,myDateSetListener, 2017, 9, 13);
                new DatePickerDialog(PendingBillsActivity.this, myDateSetListener, Integer.parseInt(date_split[2]), Integer.parseInt(date_split[1]) - 1, Integer.parseInt(date_split[0]));
        datePickerDialog.setTitle("From Date");
        datePickerDialog.show();


    }

    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(PendingBillsActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<PendingBillsDto> itemsList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(PendingBillsActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.GetPendingOTList(strings[0], strings[1], strings[2], strings[3], strings[4]);
                    result = "" + getResult;
                    if (result.contains("true")) {
//                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = getResult.getJSONArray("Value");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<PendingBillsDto>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(PendingBillsActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        if (itemsList.size() > 0) {
                            showData(itemsList);
                        } else {
                            Toast.makeText(PendingBillsActivity.this, "Bills are not available", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(PendingBillsActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(PendingBillsActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(PendingBillsActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(PendingBillsActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    public void showData(List<PendingBillsDto> itemsList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PendingBillsActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        PendingBillsAdapter orderListAdapter = new PendingBillsAdapter(itemsList, PendingBillsActivity.this);
        recyclerView.setAdapter(orderListAdapter);
        orderListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
