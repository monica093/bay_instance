package com.irca.dto;

public class GetDiscountDTO {
    String ID="";
    String DiscountTypeCode="";

    String DiscountTypeName="";
    String Value="";
    String Type="";
    String StoreID="";
    String ItemCategoryID="";



    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getDiscountTypeCode() {
        return DiscountTypeCode;
    }

    public void setDiscountTypeCode(String discountTypeCode) {
        DiscountTypeCode = discountTypeCode;
    }

    public String getDiscountTypeName() {
        return DiscountTypeName;
    }

    public void setDiscountTypeName(String discountTypeName) {
        DiscountTypeName = discountTypeName;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getStoreID() {
        return StoreID;
    }

    public void setStoreID(String storeID) {
        StoreID = storeID;
    }

    public String getItemCategoryID() {
        return ItemCategoryID;
    }

    public void setItemCategoryID(String itemCategoryID) {
        ItemCategoryID = itemCategoryID;
    }

}
