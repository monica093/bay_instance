package com.irca.dto;


public class SendModifiers {

    public String billId, itemId, modifierId, modifierDetailid, modifier;

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public String getModifierDetailid() {
        return modifierDetailid;
    }

    public void setModifierDetailid(String modifierDetailid) {
        this.modifierDetailid = modifierDetailid;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }
}
