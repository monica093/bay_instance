package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DependentDetails implements Serializable {

    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("MemberID")
    @Expose
    private String memberID;
    @SerializedName("DependantID")
    @Expose
    private String dependantID;
    @SerializedName("DependantAccountnumber")
    @Expose
    private String dependantAccountnumber;
    @SerializedName("Salutation")
    @Expose
    private String salutation;
    @SerializedName("DependantName")
    @Expose
    private String dependantName;
    @SerializedName("MaritialStatus")
    @Expose
    private String maritialStatus;
    @SerializedName("Sex")
    @Expose
    private String sex;
    @SerializedName("Relation")
    @Expose
    private String relation;
    @SerializedName("MemberActive")
    @Expose
    private String memberActive;
    @SerializedName("DateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("ConfirmationDate")
    @Expose
    private String confirmationDate;
    @SerializedName("ExpireDate")
    @Expose
    private String expireDate;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("MemberType")
    @Expose
    private String memberType;
    @SerializedName("Age")
    @Expose
    private String age;
    @SerializedName("RenewalDate")
    @Expose
    private String renewalDate;
    @SerializedName("Remarks")
    @Expose
    private String remarks;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getDependantID() {
        return dependantID;
    }

    public void setDependantID(String dependantID) {
        this.dependantID = dependantID;
    }

    public String getDependantAccountnumber() {
        return dependantAccountnumber;
    }

    public void setDependantAccountnumber(String dependantAccountnumber) {
        this.dependantAccountnumber = dependantAccountnumber;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getDependantName() {
        return dependantName;
    }

    public void setDependantName(String dependantName) {
        this.dependantName = dependantName;
    }

    public String getMaritialStatus() {
        return maritialStatus;
    }

    public void setMaritialStatus(String maritialStatus) {
        this.maritialStatus = maritialStatus;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getMemberActive() {
        return memberActive;
    }

    public void setMemberActive(String memberActive) {
        this.memberActive = memberActive;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(String confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(String renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}

