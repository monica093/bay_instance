package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TableTransferDetails implements Serializable {


    @SerializedName("BillID")
    @Expose
    private String billID;
    @SerializedName("ItemID")
    @Expose
    private String itemID;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("OTNote")
    @Expose
    private String oTNote;
    @SerializedName("BillDate")
    @Expose
    private String billDate;
    @SerializedName("TableID")
    @Expose
    private String TableID;
    @SerializedName("TableNumber")
    @Expose
    private String TableNumber;
    @SerializedName("itemname")
    @Expose
    private String itemname;
    @SerializedName("MemberID")
    @Expose
    private String memberID;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("membername")
    @Expose
    private String membername;
    @SerializedName("accountnumber")
    @Expose
    private String accountnumber;
    @SerializedName("BillNumber")
    @Expose
    private String billNumber;
    @SerializedName("OTNo")
    @Expose
    private String oTNo;
    @SerializedName("isClick")
    @Expose
    private boolean isClick;
    @SerializedName("ItemCategoryID")
    @Expose
    private String itemCategoryID;

    @SerializedName("Paxcount")
    @Expose
    private String paxCount;

    public String getoTNote() {
        return oTNote;
    }

    public void setoTNote(String oTNote) {
        this.oTNote = oTNote;
    }

    public String getPaxCount() {
        return paxCount;
    }

    public void setPaxCount(String paxCount) {
        this.paxCount = paxCount;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public String getTableID() {
        return TableID;
    }

    public void setTableID(String tableID) {
        TableID = tableID;
    }

    public String getTableNumber() {
        return TableNumber;
    }

    public void setTableNumber(String tableNumber) {
        TableNumber = tableNumber;
    }

    public String getBillID() {
        return billID;
    }

    public void setBillID(String billID) {
        this.billID = billID;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getOTNo() {
        return oTNo;
    }

    public void setOTNo(String oTNo) {
        this.oTNo = oTNo;
    }

    public String getItemCategoryID() {
        return itemCategoryID;
    }

    public void setItemCategoryID(String itemCategoryID) {
        this.itemCategoryID = itemCategoryID;
    }

}
