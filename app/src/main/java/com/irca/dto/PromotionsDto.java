package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PromotionsDto implements Serializable {


    @SerializedName("PromotionId")
    @Expose
    private String promotionId;
    @SerializedName("PromotionName")
    @Expose
    private String promotionName;
    @SerializedName("ItemID")
    @Expose
    private String itemID;
    @SerializedName("Mainitem")
    @Expose
    private String mainItem;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Rate")
    @Expose
    private String rate;
    @SerializedName("FromDate")
    @Expose
    private String fromDate;
    @SerializedName("ToDate")
    @Expose
    private String toDate;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("OfferPromotionId")
    @Expose
    private String offerPromotionId;
    @SerializedName("FreeItemId")
    @Expose
    private String freeItemId;
    @SerializedName("FreeQty")
    @Expose
    private String freeQty;
    @SerializedName("POSID")
    @Expose
    private String pOSID;
    @SerializedName("FreeItemCode")
    @Expose
    private String freeItemCode;
    @SerializedName("FreeItemName")
    @Expose
    private String freeItemName;

    public String getMainItem() {
        return mainItem;
    }

    public void setMainItem(String mainItem) {
        this.mainItem = mainItem;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getOfferPromotionId() {
        return offerPromotionId;
    }

    public void setOfferPromotionId(String offerPromotionId) {
        this.offerPromotionId = offerPromotionId;
    }

    public String getFreeItemId() {
        return freeItemId;
    }

    public void setFreeItemId(String freeItemId) {
        this.freeItemId = freeItemId;
    }

    public String getFreeQty() {
        return freeQty;
    }

    public void setFreeQty(String freeQty) {
        this.freeQty = freeQty;
    }

    public String getPOSID() {
        return pOSID;
    }

    public void setPOSID(String pOSID) {
        this.pOSID = pOSID;
    }

    public String getFreeItemCode() {
        return freeItemCode;
    }

    public void setFreeItemCode(String freeItemCode) {
        this.freeItemCode = freeItemCode;
    }

    public String getFreeItemName() {
        return freeItemName;
    }

    public void setFreeItemName(String freeItemName) {
        this.freeItemName = freeItemName;
    }


}
