package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MemberMainDto implements Serializable {


    @SerializedName("Table")
    @Expose
    private List<MemberDetails> memberDetailsList = new ArrayList<>();
    @SerializedName("Table1")
    @Expose
    private List<DependentDetails> dependentDetailsList = new ArrayList<>();

    @SerializedName("Table2")
    @Expose
    private List<MemberItemGuest> MemberItemGuestList = new ArrayList<>();

    @SerializedName("Table3")
    @Expose
    private List<MemberNotes>  MemberNotesList = new ArrayList<>();



    public List<MemberItemGuest> getMemberItemGuestList() {
        return MemberItemGuestList;
    }

    public void setMemberItemGuestList(List<MemberItemGuest> memberItemGuestList) {
        MemberItemGuestList = memberItemGuestList;
    }

    public List<MemberNotes> getMemberNotesList() {
        return MemberNotesList;
    }

    public void setMemberNotesList(List<MemberNotes> memberNotesList) {
        MemberNotesList = memberNotesList;
    }

    public List<MemberDetails> getMemberDetailsList() {
        return memberDetailsList;
    }

    public void setMemberDetailsList(List<MemberDetails> memberDetailsList) {
        this.memberDetailsList = memberDetailsList;
    }

    public List<DependentDetails> getDependentDetailsList() {
        return dependentDetailsList;
    }

    public void setDependentDetailsList(List<DependentDetails> dependentDetailsList) {
        this.dependentDetailsList = dependentDetailsList;
    }
}
