package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OTItemDto implements Serializable {

    @SerializedName("BillID")
    @Expose
    private String billID;
    @SerializedName("MemberID")
    @Expose
    private String memberID;
    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("MemberName")
    @Expose
    private String memberName;
    @SerializedName("BillMode")
    @Expose
    private String billMode;
    @SerializedName("BillType")
    @Expose
    private String billType;
    @SerializedName("ReferenceNumber")
    @Expose
    private String referenceNumber;
    @SerializedName("ContractorsID")
    @Expose
    private String contractorsID;
    @SerializedName("OTNo")
    @Expose
    private String oTNo;
    @SerializedName("MType")
    @Expose
    private String mType;
    @SerializedName("TableNo")
    @Expose
    private String tableNo;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("ItemID")
    @Expose
    private String itemID;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("ItemSerialNo")
    @Expose
    private String itemSerialNo;

    public String getBillID() {
        return billID;
    }

    public void setBillID(String billID) {
        this.billID = billID;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getBillMode() {
        return billMode;
    }

    public void setBillMode(String billMode) {
        this.billMode = billMode;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getContractorsID() {
        return contractorsID;
    }

    public void setContractorsID(String contractorsID) {
        this.contractorsID = contractorsID;
    }

    public String getOTNo() {
        return oTNo;
    }

    public void setOTNo(String oTNo) {
        this.oTNo = oTNo;
    }

    public String getMType() {
        return mType;
    }

    public void setMType(String mType) {
        this.mType = mType;
    }

    public String getTableNo() {
        return tableNo;
    }

    public void setTableNo(String tableNo) {
        this.tableNo = tableNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getItemSerialNo() {
        return itemSerialNo;
    }

    public void setItemSerialNo(String itemSerialNo) {
        this.itemSerialNo = itemSerialNo;
    }


}
