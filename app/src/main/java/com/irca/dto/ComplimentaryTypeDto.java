package com.irca.dto;

import java.io.Serializable;

public class ComplimentaryTypeDto implements Serializable {
    String ID="";

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getComplimentaryTypeName() {
        return ComplimentaryTypeName;
    }

    public void setComplimentaryTypeName(String complimentaryTypeName) {
        ComplimentaryTypeName = complimentaryTypeName;
    }

    String ComplimentaryTypeName="";
}
