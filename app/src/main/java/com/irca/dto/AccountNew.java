package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AccountNew implements Serializable {

    @SerializedName("BillID")
    @Expose
    private String billID;

    public String getContractorsID() {
        return ContractorsID;
    }

    public void setContractorsID(String contractorsID) {
        ContractorsID = contractorsID;
    }

    @SerializedName("ContractorsID")
    @Expose
    private String ContractorsID;
    @SerializedName("POSID")
    @Expose
    private String pOSID;
    @SerializedName("ACCNUM")
    @Expose
    private String aCCNUM;
    @SerializedName("ACC_NAME")
    @Expose
    private String aCCNAME;
    @SerializedName("TODAYSDATE")
    @Expose
    private String tODAYSDATE;
    @SerializedName("CARDTYPE")
    @Expose
    private String cARDTYPE;
    @SerializedName("OPENING_BALANCE")
    @Expose
    private String oPENINGBALANCE;
    @SerializedName("DEBIT_BALANCE")
    @Expose
    private String dEBITBALANCE;
    @SerializedName("PaCODE")
    @Expose
    private String paCODE;
    @SerializedName("PaNAME")
    @Expose
    private String paNAME;
    @SerializedName("PaID")
    @Expose
    private String paID;
    @SerializedName("BILLMODE")
    @Expose
    private String bILLMODE;
    @SerializedName("BILLMODE_TYPE")
    @Expose
    private String bILLMODETYPE;
    @SerializedName("TABLENO")
    @Expose
    private String tABLENO;
    @SerializedName("AC_NO")
    @Expose
    private String aCNO;
    @SerializedName("ISOTD")
    @Expose
    private String iSOTD;
    @SerializedName("PAX_COUNT")
    @Expose
    private String pAXCOUNT;
    @SerializedName("STEWARD")
    @Expose
    private String sTEWARD;
    @SerializedName("ACCESS_TYPE")
    @Expose
    private String aCCESSTYPE;
    @SerializedName("MemberType")
    @Expose
    private String memberType;
    @SerializedName("memberID")
    @Expose
    private String memberID;

    public String getSmartCardSerialNo() {
        return SmartCardSerialNo;
    }

    public void setSmartCardSerialNo(String smartCardSerialNo) {
        SmartCardSerialNo = smartCardSerialNo;
    }

    @SerializedName("SmartCardSerialNo")
    @Expose
    private String SmartCardSerialNo;

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getBillID() {
        return billID;
    }

    public void setBillID(String billID) {
        this.billID = billID;
    }

    public String getPOSID() {
        return pOSID;
    }

    public void setPOSID(String pOSID) {
        this.pOSID = pOSID;
    }

    public String getACCNUM() {
        return aCCNUM;
    }

    public void setACCNUM(String aCCNUM) {
        this.aCCNUM = aCCNUM;
    }

    public String getACCNAME() {
        return aCCNAME;
    }

    public void setACCNAME(String aCCNAME) {
        this.aCCNAME = aCCNAME;
    }

    public String getTODAYSDATE() {
        return tODAYSDATE;
    }

    public void setTODAYSDATE(String tODAYSDATE) {
        this.tODAYSDATE = tODAYSDATE;
    }

    public String getCARDTYPE() {
        return cARDTYPE;
    }

    public void setCARDTYPE(String cARDTYPE) {
        this.cARDTYPE = cARDTYPE;
    }

    public String getOPENINGBALANCE() {
        return oPENINGBALANCE;
    }

    public void setOPENINGBALANCE(String oPENINGBALANCE) {
        this.oPENINGBALANCE = oPENINGBALANCE;
    }

    public String getDEBITBALANCE() {
        return dEBITBALANCE;
    }

    public void setDEBITBALANCE(String dEBITBALANCE) {
        this.dEBITBALANCE = dEBITBALANCE;
    }

    public String getPaCODE() {
        return paCODE;
    }

    public void setPaCODE(String paCODE) {
        this.paCODE = paCODE;
    }

    public String getPaNAME() {
        return paNAME;
    }

    public void setPaNAME(String paNAME) {
        this.paNAME = paNAME;
    }

    public String getPaID() {
        return paID;
    }

    public void setPaID(String paID) {
        this.paID = paID;
    }

    public String getBILLMODE() {
        return bILLMODE;
    }

    public void setBILLMODE(String bILLMODE) {
        this.bILLMODE = bILLMODE;
    }

    public String getBILLMODETYPE() {
        return bILLMODETYPE;
    }

    public void setBILLMODETYPE(String bILLMODETYPE) {
        this.bILLMODETYPE = bILLMODETYPE;
    }

    public String getTABLENO() {
        return tABLENO;
    }

    public void setTABLENO(String tABLENO) {
        this.tABLENO = tABLENO;
    }

    public String getACNO() {
        return aCNO;
    }

    public void setACNO(String aCNO) {
        this.aCNO = aCNO;
    }

    public String getISOTD() {
        return iSOTD;
    }

    public void setISOTD(String iSOTD) {
        this.iSOTD = iSOTD;
    }

    public String getPAXCOUNT() {
        return pAXCOUNT;
    }

    public void setPAXCOUNT(String pAXCOUNT) {
        this.pAXCOUNT = pAXCOUNT;
    }

    public String getSTEWARD() {
        return sTEWARD;
    }

    public void setSTEWARD(String sTEWARD) {
        this.sTEWARD = sTEWARD;
    }

    public String getACCESSTYPE() {
        return aCCESSTYPE;
    }

    public void setACCESSTYPE(String aCCESSTYPE) {
        this.aCCESSTYPE = aCCESSTYPE;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }
}
