package com.irca.tax;



public class StoreClass {
    public String id;
    public String Storecode;
    public String storename;
    public String categoryId;
    public String GSTNo;
//    {
//        "Successful": true,
//            "Value": [
//        {
//            "ID": "1",
//                "StoreCode": "MS",
//                "StoreName": "Main Store",
//                "ItemCategoryID": "1"
//        },
//        {
//            "ID": "2",
//                "StoreCode": "OT",
//                "StoreName": "Other ",
//                "ItemCategoryID": "1"
//        }
//  ]
//    }


    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getStorecode() {
        return Storecode;
    }

    public void setStorecode(String storecode) {
        Storecode = storecode;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getGSTNo() {
        return GSTNo;
    }

    public void setGSTNo(String GSTNo) {
        this.GSTNo = GSTNo;
    }
}
