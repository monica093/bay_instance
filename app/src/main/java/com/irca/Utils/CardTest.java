package com.irca.Utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.acs.audiojack.AudioJackReader;
import com.acs.audiojack.Result;
import com.irca.cosmo.R;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;

import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Manoch Richard on 25-Jul-17.
 */

public class CardTest extends AppCompatActivity {

    private boolean mResultReady;//power
    private AudioManager mAudioManager;
    private AudioJackReader mReader;
    private Context mContext = this;
    private ProgressDialog mProgress;

    private boolean mPiccResponseApduReady;// transmit to check apdu return values
    private Object mResponseEvent = null;

    public String finalValue = "";
    public String waiterid;
    public static String waiternamecode;
    public int manoch = 0;
    public int overAll = 0;
    public int k = 0;
    public int position;
    public int positionb;
    public String itemValue;
    //    public String daniel = "FF 82 00 00 06 FF FF FF FF FF FF," +
//            "FF 86 00 00 05 01 00 04 60 00," +
//            "FF B0 00 04 10," +
//            "FF 86 00 00 05 01 00 05 60 00," +
//            "FF B0 00 05 10,"+
//            "FF 86 00 00 05 01 00 06 60 00," +
//            "FF B0 00 06 10 ";
    //public  String daniel1="FF 86 00 00 05 01 00 05 60 00,FF B0 00 05 10 ";
    //public  String daniel2="FF 86 00 00 05 01 00 05 60 00,FF B0 00 06 10 ";
    public String daniel = "FF 82 00 00 06 FF FF FF FF FF FF," +
            "FF 86 00 00 05 01 00 04 60 00," +
            "FF B0 00 04 10";
    //"FF 86 00 00 05 01 00 05 60 00," +
    //"FF B0 00 05 10,"+
    //"FF 86 00 00 05 01 00 06 60 00," +
   // "FF B0 00 06 10 ";

    private int mPiccTimeout = 1;
    private int mPiccCardType = 143;
    private byte[] mPiccCommandApdu;// transmit

    private byte[] mPiccResponseApdu;
    private byte[] mPiccRfConfig = new byte[19];
    private Result mResult;
    AudioManager audio;
    EditText memberAn, cardvalue;
    FloatingActionButton bot;
    private final BroadcastReceiver mHeadsetPlugReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {

                boolean plugged = (intent.getIntExtra("state", 0) == 1);

                /* Mute the audio output if the reader is unplugged. */
                mReader.setMute(!plugged);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_test);
        bot = (FloatingActionButton) findViewById(R.id.bot);
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mReader = new AudioJackReader(mAudioManager, true);
        memberAn = (EditText) findViewById(R.id.editText_m1);
        cardvalue = (EditText) findViewById(R.id.editText_value);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(mHeadsetPlugReceiver, filter);

        mProgress = new ProgressDialog(mContext);
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        String piccRfConfigString = "";
        if ((piccRfConfigString == null) || piccRfConfigString.equals("")
                || (toByteArray(piccRfConfigString, mPiccRfConfig) != 19)) {

            piccRfConfigString = "07 85 85 85 85 85 85 85 85 69 69 69 69 69 69 69 69 3F 3F";
            toByteArray(piccRfConfigString, mPiccRfConfig);
        }
        piccRfConfigString = toHexString(mPiccRfConfig);

         /* Set the PICC response APDU callback. */
        mReader.setOnPiccResponseApduAvailableListener(new CardTest.OnPiccResponseApduAvailableListener());

        bot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio.setStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                        0);
                mReader.piccPowerOff();
                mReader.sleep();
                finalValue = "";
                manoch = 0;
                k = 0;
                memberAn.setText("");
                mPiccResponseApdu = null;
                mResponseEvent = null;
                mReader.start();
                mResponseEvent = new Object();
                overAll = 0;
                if (!checkResetVolume()) {
                } else {
                    mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    mProgress.setMessage("Reading Card please wait.. ");
                    mProgress.show();
                    new Thread(new Transmit()).start();
                }
            }
        });
    }

    public class Transmit implements Runnable {

        @Override
        public void run() {

            mReader.reset();
            if (!mReader.piccPowerOn(5, 143)) {
                //showRequestQueueError();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Reader is not available", Toast.LENGTH_LONG).show();
                        mProgress.dismiss();
                    }
                });


            } else {
                          /* Transmit the command APDU. */
                while (manoch <= 2) {
                // while (manoch <= 4) {
                //while (manoch <= 6) {
                    mPiccResponseApduReady = false;
                    mResultReady = false;
                    String india[] = null;
                    india = daniel.split(",");
                    mPiccCommandApdu = toByteArray(india[k]);
                    if (!mReader.piccTransmit(mPiccTimeout, mPiccCommandApdu)) {
                                     /* Show the request queue error. */
                        //showRequestQueueError();
                        Toast.makeText(mContext, "Read again ", Toast.LENGTH_LONG).show();

                    } else {

                                    /* Show the PICC response APDU. */
                        //showPiccResponseApdu();
                        synchronized (mResponseEvent) {
                                     /* Wait for the PICC response APDU. */

                            while (!mPiccResponseApduReady && !mResultReady) {

                                try {
                                    mResponseEvent.wait(700);
                                } catch (InterruptedException e) {
                                }
                                break;
                            }

                            if (mPiccResponseApduReady) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                                    /* Show the PICC response APDU. */
                                        String current = toHexString(mPiccResponseApdu);
                                        if (current.contains("90 00")) {

                                            if (manoch == 3) {
                                                finalValue = finalValue + current;
                                                finalValue = finalValue.replaceAll("\\s+", "");
                                                finalValue = hexToString(finalValue);
                                                cardvalue.setText(finalValue);
                                                mReader.piccPowerOff();
                                                mReader.sleep();
                                                //getCardDetails(finalValue);
                                            } else if (manoch == 5) {
                                                try {

                                                    finalValue = finalValue + current;
                                                      /*  finalValue = finalValue.replaceAll("\\s+", "");
                                                        finalValue = hexToString(finalValue);
                                                        if (finalValue.contains("$"))
                                                        {
                                                            String ss[] = finalValue.split(Pattern.quote("$"));
                                                           // if (finalValue.contains("CLUB"))

                                                            if (finalValue.contains("DP")){


                                                                memberAn.setText(ss[3] + ":DEPENDENT CARD");
                                                                member = ss[3] + ":DEPENDENT CARD";
                                                                cardType = "DEPENDENT CARD";
                                                                order.setVisibility(View.VISIBLE);

                                                            }

                                                            else if (finalValue.contains("ICLB"))
                                                            {
                                                                memberAn.setText(ss[2] + ":CLUB CARD");
                                                                member = ss[2] + ":CLUB CARD";
                                                                cardType = "CLUB CARD";
                                                                order.setVisibility(View.VISIBLE);

                                                            }
                                                            else if (finalValue.contains("ICSH"))
                                                            {
                                                                memberAn.setText(ss[1] + ":CASH CARD");
                                                                member = ss[1] + ":CASH CARD";
                                                                cardType = "CASH CARD";
                                                                order.setVisibility(View.VISIBLE);

                                                            }
    //                                                        else if (finalValue.contains("noom"))
    //                                                        {
    //                                                            memberAn.setText(ss[1] + ":NEW ROOM CARD");
    //                                                            member = ss[1] + ":NEW ROOM CARD";
    //                                                            cardType = "NEW ROOM CARD";
    //                                                            order.setVisibility(View.VISIBLE);
    //                                                        }
                                                            else if (finalValue.contains("room"))
                                                            {
                                                                memberAn.setText(ss[1] + ":ROOM CARD");
                                                                member = ss[1] + ":ROOM CARD";
                                                                cardType = "ROOM CARD";
                                                                order.setVisibility(View.VISIBLE);

                                                          //  } else if (finalValue.contains("BGCCC"))
                                                            }
    //                                                        else if (finalValue.contains("TMP"))
    //                                                        {
    //                                                            memberAn.setText(ss[2] + ":TEMP CARD");
    //                                                            member = ss[2] + ":TEMP CARD";
    //                                                            cardType = "TEMP CARD";
    //                                                            order.setVisibility(View.VISIBLE);
    //
    //                                                        }
                                                            else if (finalValue.contains("DUPLC"))
                                                            {
                                                                memberAn.setText(ss[3] + ":MEMBER DUPL CARD");
                                                                member = ss[3] + ":MEMBER DUPL CARD";
                                                                cardType = "MEMBER DUPL CARD";
                                                                order.setVisibility(View.VISIBLE);

                                                            }else  if (finalValue.contains("0$"))
                                                            {
                                                                memberAn.setText(ss[3] + ":MEMBER CARD");
                                                                member = ss[3] + ":MEMBER CARD";
                                                                cardType = "MEMBER CARD";
                                                                order.setVisibility(View.VISIBLE);
                                                            }  else
                                                            {
                                                                memberAn.setText("Error:" + finalValue);
                                                                member = "";
                                                                order.setVisibility(View.INVISIBLE);
                                                            }
                                                            mReader.piccPowerOff();
                                                            mReader.sleep();
                                                        }*/
                                                } catch (Exception e) {
                                                    Toast.makeText(mContext, "Error" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                }

                                            } else if (manoch == 7) {
                                                try {

                                                    finalValue = finalValue + current;
                                                    finalValue = finalValue.replaceAll("\\s+", "");
                                                    finalValue = hexToString(finalValue);
                                                    getCardDetails(finalValue);
//                                                    if (finalValue.contains("$")) {
//                                                        String ss[] = finalValue.split(Pattern.quote("$"));
//                                                        // if (finalValue.contains("CLUB"))
//
//                                                        if (ss[0].contains("DP")) {
//
//
//                                                            memberAn.setText(ss[1] + ":DEPENDENT CARD");
//                                                            //member = ss[1] + ":DEPENDENT CARD";
//                                                            //cardType = "DEPENDENT CARD";
//                                                            //order.setVisibility(View.VISIBLE);
//
//                                                        } else if (finalValue.contains("ICLB")) {
//                                                            memberAn.setText(ss[2] + ":CLUB CARD");
//                                                           // member = ss[2] + ":CLUB CARD";
//                                                           // cardType = "CLUB CARD";
//                                                            //order.setVisibility(View.VISIBLE);
//
//                                                        } else if (finalValue.contains("ICSH")) {
//                                                            memberAn.setText(ss[1] + ":CASH CARD");
//                                                           // member = ss[1] + ":CASH CARD";
//                                                           // cardType = "CASH CARD";
//                                                           // order.setVisibility(View.VISIBLE);
//
//                                                        } else if (finalValue.contains("SMRT")) {
//                                                            memberAn.setText(ss[1] + ":SMART CARD");
//                                                           // member = ss[1] + ":SMART CARD";
//                                                           // cardType = "SMART CARD";
//                                                            //order.setVisibility(View.VISIBLE);
//                                                        }
//                                                        //                                                        else if (finalValue.contains("noom"))
//                                                        //                                                        {
//                                                        //                                                            memberAn.setText(ss[1] + ":NEW ROOM CARD");
//                                                        //                                                            member = ss[1] + ":NEW ROOM CARD";
//                                                        //                                                            cardType = "NEW ROOM CARD";
//                                                        //                                                            order.setVisibility(View.VISIBLE);
//                                                        //                                                        }
//                                                        else if (finalValue.contains("room")) {
//                                                            memberAn.setText(ss[1] + ":ROOM CARD");
//                                                           // member = ss[1] + ":ROOM CARD";
//                                                           // cardType = "ROOM CARD";
//                                                           // order.setVisibility(View.VISIBLE);
//
//                                                            //  } else if (finalValue.contains("BGCCC"))
//                                                        }
//                                                        //                                                        else if (finalValue.contains("TMP"))
//                                                        //                                                        {
//                                                        //                                                            memberAn.setText(ss[2] + ":TEMP CARD");
//                                                        //                                                            member = ss[2] + ":TEMP CARD";
//                                                        //                                                            cardType = "TEMP CARD";
//                                                        //                                                            order.setVisibility(View.VISIBLE);
//                                                        //
//                                                        //                                                        }
//                                                        else if (finalValue.contains("DUPLC")) {
//                                                            memberAn.setText(ss[3] + ":MEMBER DUPL CARD");
//                                                           // member = ss[3] + ":MEMBER DUPL CARD";
//                                                          //  cardType = "MEMBER DUPL CARD";
//                                                           // order.setVisibility(View.VISIBLE);
//
//                                                        } else if (finalValue.contains("0$")) {
//
//                                                            if (ss[3].equals("CLB")) {
//                                                                memberAn.setText(ss[3] + ":CLUB CARD");
//                                                               // member = ss[3] + ":CLUB CARD";
//                                                              //  cardType = "CLUB CARD";
//                                                              //  order.setVisibility(View.VISIBLE);
//
//                                                            } else {
//                                                                memberAn.setText(ss[3] + ":MEMBER CARD,"+finalValue);
//                                                               // member = ss[3] + ":MEMBER CARD";
//                                                               // cardType = "MEMBER CARD";
//                                                               // order.setVisibility(View.VISIBLE);
//                                                            }
//
//                                                        } else {
//                                                            memberAn.setText("Error:" + finalValue);
//                                                            //member = "";
//                                                           // order.setVisibility(View.INVISIBLE);
//                                                        }
//                                                        mReader.piccPowerOff();
//                                                        mReader.sleep();
//                                                    }
                                                } catch (Exception e) {
                                                    Toast.makeText(mContext, "Error  1" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                            manoch++;
                                            k++;
                                            overAll++;
                                        } else {
                                            mReader.piccPowerOn(5, 143);
                                            overAll++;

                                        }
                                    }
                                });

                            } else if (mResultReady) {

                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                                    /* Show the result. */
                                        Toast.makeText(mContext,
                                                toErrorCodeString(mResult.getErrorCode()),
                                                Toast.LENGTH_LONG).show();
                                        mProgress.dismiss();
                                    }
                                });

                            } else {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        // finalValue=finalValue+"-11";
                                                     /* Show the timeout. */
                                        overAll++;
                                        mReader.piccPowerOn(5, 143);
                                        if (overAll > 8) {
                                            Toast.makeText(mContext, "The operation timed out Read again ", Toast.LENGTH_LONG).show();
                                            mReader.piccPowerOff();
                                            mReader.sleep();
                                        }
                                    }
                                });
                            }

                            mPiccResponseApduReady = false;
                            mResultReady = false;
                        }
                    }

                    if (overAll > 8) {
                        mReader.piccPowerOff();
                        mReader.sleep();
                        mProgress.dismiss();
                        break;
                    }
                }
                            /* Hide the progress. */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mProgress.dismiss();
                    }

                });
            }
        }


    }

    private class OnPiccResponseApduAvailableListener implements AudioJackReader.OnPiccResponseApduAvailableListener {

        @Override
        public void onPiccResponseApduAvailable(AudioJackReader reader,
                                                byte[] responseApdu) {

            synchronized (mResponseEvent) {
                /* Store the PICC response APDU. */
                mPiccResponseApdu = new byte[responseApdu.length];
                System.arraycopy(responseApdu, 0, mPiccResponseApdu, 0,
                        responseApdu.length);
                /* Trigger the response event. */
                mPiccResponseApduReady = true;
                mResponseEvent.notifyAll();
            }
        }
    }

    @Override
    protected void onDestroy() {
        /* Unregister the headset plug receiver. */
        unregisterReceiver(mHeadsetPlugReceiver);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mReader.start();
        } catch (Exception e) {
//            String ss = e.getMessage().toString();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            mReader.start();
        } catch (Exception e) {

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            mReader.stop();
            mProgress.dismiss();
        } catch (Exception e) {

        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            mProgress.dismiss();
            mReader.stop();
        } catch (Exception e) {

        }

    }

    private String toHexString(byte[] buffer) {

        String bufferString = "";

        if (buffer != null) {

            for (int i = 0; i < buffer.length; i++) {

                String hexChar = Integer.toHexString(buffer[i] & 0xFF);
                if (hexChar.length() == 1) {
                    hexChar = "0" + hexChar;
                }

                bufferString += hexChar.toUpperCase(Locale.US) + " ";
            }
        }

        return bufferString;
    }

    private boolean checkResetVolume() {

        boolean ret = true;

        int currentVolume = mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);

        int maxVolume = mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        if (currentVolume < maxVolume) {

            showMessageDialog(R.string.info, R.string.message_reset_info_volume);
            ret = false;
        }

        return ret;
    }

    private void showMessageDialog(int titleId, int messageId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setMessage(messageId)
                .setTitle(titleId)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });

        builder.show();
    }

    private String toErrorCodeString(int errorCode) {

        String errorCodeString = null;

        switch (errorCode) {
            case Result.ERROR_SUCCESS:
                errorCodeString = "The operation completed successfully.";
                break;
            case Result.ERROR_INVALID_COMMAND:
                errorCodeString = "The command is invalid.";
                break;
            case Result.ERROR_INVALID_PARAMETER:
                errorCodeString = "The parameter is invalid.";
                break;
            case Result.ERROR_INVALID_CHECKSUM:
                errorCodeString = "The checksum is invalid.";
                break;
            case Result.ERROR_INVALID_START_BYTE:
                errorCodeString = "The start byte is invalid.";
                break;
            case Result.ERROR_UNKNOWN:
                errorCodeString = "The error is unknown.";
                break;
            case Result.ERROR_DUKPT_OPERATION_CEASED:
                errorCodeString = "The DUKPT operation is ceased.";
                break;
            case Result.ERROR_DUKPT_DATA_CORRUPTED:
                errorCodeString = "The DUKPT data is corrupted.";
                break;
            case Result.ERROR_FLASH_DATA_CORRUPTED:
                errorCodeString = "The flash data is corrupted.";
                break;
            case Result.ERROR_VERIFICATION_FAILED:
                errorCodeString = "The verification is failed.";
                break;
            case Result.ERROR_PICC_NO_CARD:
                errorCodeString = "No card in PICC slot.";
                break;
            default:
                errorCodeString = "Error communicating with reader.";
                break;
        }

        return errorCodeString;
    }

    private int toByteArray(String hexString, byte[] byteArray) {

        char c = 0;
        boolean first = true;
        int length = 0;
        int value = 0;
        int i = 0;

        for (i = 0; i < hexString.length(); i++) {

            c = hexString.charAt(i);
            if ((c >= '0') && (c <= '9')) {
                value = c - '0';
            } else if ((c >= 'A') && (c <= 'F')) {
                value = c - 'A' + 10;
            } else if ((c >= 'a') && (c <= 'f')) {
                value = c - 'a' + 10;
            } else {
                value = -1;
            }

            if (value >= 0) {

                if (first) {

                    byteArray[length] = (byte) (value << 4);

                } else {

                    byteArray[length] |= value;
                    length++;
                }

                first = !first;
            }

            if (length >= byteArray.length) {
                break;
            }
        }

        return length;
    }

    private byte[] toByteArray(String hexString) {

        byte[] byteArray = null;
        int count = 0;
        char c = 0;
        int i = 0;

        boolean first = true;
        int length = 0;
        int value = 0;

        // Count number of hex characters
        for (i = 0; i < hexString.length(); i++) {

            c = hexString.charAt(i);
            if (c >= '0' && c <= '9' || c >= 'A' && c <= 'F' || c >= 'a'
                    && c <= 'f') {
                count++;
            }
        }

        byteArray = new byte[(count + 1) / 2];
        for (i = 0; i < hexString.length(); i++) {

            c = hexString.charAt(i);
            if (c >= '0' && c <= '9') {
                value = c - '0';
            } else if (c >= 'A' && c <= 'F') {
                value = c - 'A' + 10;
            } else if (c >= 'a' && c <= 'f') {
                value = c - 'a' + 10;
            } else {
                value = -1;
            }

            if (value >= 0) {

                if (first) {

                    byteArray[length] = (byte) (value << 4);

                } else {

                    byteArray[length] |= value;
                    length++;
                }

                first = !first;
            }
        }

        return byteArray;
    }

    public String hexToString(String hex1) {

        StringBuilder sb = new StringBuilder();
        String hex2 = hex1.replaceAll("00", "");
        String hex = hex2.replaceAll("90", "");
        char[] hexData = hex.toCharArray();
        for (int count = 0; count < hexData.length - 1; count += 2) {
            int firstDigit = Character.digit(hexData[count], 16);
            int lastDigit = Character.digit(hexData[count + 1], 16);
            int decimal = firstDigit * 16 + lastDigit;
            sb.append((char) decimal);
        }
        return sb.toString();
    }

    public String getCardDetails(String finalValue) {
        if (finalValue.contains("$")) {
            String ss[] = finalValue.split(Pattern.quote("$"));
            // if (finalValue.contains("CLUB"))

            if (ss[0].contains("DP")) {


                memberAn.setText(ss[1] + ":DEPENDENT CARD");
                cardvalue.setText(finalValue);
                //member = ss[1] + ":DEPENDENT CARD";
                //cardType = "DEPENDENT CARD";
                //order.setVisibility(View.VISIBLE);

            } else if (finalValue.contains("ICLB")) {
                memberAn.setText(ss[2] + ":CLUB CARD");
                cardvalue.setText(finalValue);
                // member = ss[2] + ":CLUB CARD";
                // cardType = "CLUB CARD";
                //order.setVisibility(View.VISIBLE);

            } else if (finalValue.contains("ICSH")) {
                memberAn.setText(ss[1] + ":CASH CARD");
                cardvalue.setText(finalValue);
                // member = ss[1] + ":CASH CARD";
                // cardType = "CASH CARD";
                // order.setVisibility(View.VISIBLE);

            } else if (finalValue.contains("SMRT")) {
                memberAn.setText(ss[1] + ":SMART CARD");
                cardvalue.setText(finalValue);
                // member = ss[1] + ":SMART CARD";
                // cardType = "SMART CARD";
                //order.setVisibility(View.VISIBLE);
            }
            //                                                        else if (finalValue.contains("noom"))
            //                                                        {
            //                                                            memberAn.setText(ss[1] + ":NEW ROOM CARD");
            //                                                            member = ss[1] + ":NEW ROOM CARD";
            //                                                            cardType = "NEW ROOM CARD";
            //                                                            order.setVisibility(View.VISIBLE);
            //                                                        }
            else if (finalValue.contains("room")) {
                memberAn.setText(ss[1] + ":ROOM CARD");
                cardvalue.setText(finalValue);
                // member = ss[1] + ":ROOM CARD";
                // cardType = "ROOM CARD";
                // order.setVisibility(View.VISIBLE);

                //  } else if (finalValue.contains("BGCCC"))
            }
            //                                                        else if (finalValue.contains("TMP"))
            //                                                        {
            //                                                            memberAn.setText(ss[2] + ":TEMP CARD");
            //                                                            member = ss[2] + ":TEMP CARD";
            //                                                            cardType = "TEMP CARD";
            //                                                            order.setVisibility(View.VISIBLE);
            //
            //                                                        }
            else if (finalValue.contains("DUPLC")) {
                memberAn.setText(ss[3] + ":MEMBER DUPL CARD");
                cardvalue.setText(finalValue);
                // member = ss[3] + ":MEMBER DUPL CARD";
                //  cardType = "MEMBER DUPL CARD";
                // order.setVisibility(View.VISIBLE);

            } else if (finalValue.contains("0$")) {

                if (ss[3].equals("CLB")) {
                    memberAn.setText(ss[3] + ":CLUB CARD");
                    cardvalue.setText(finalValue);
                    // member = ss[3] + ":CLUB CARD";
                    //  cardType = "CLUB CARD";
                    //  order.setVisibility(View.VISIBLE);

                } else {
                    memberAn.setText(ss[3] + ":MEMBER CARD");
                    cardvalue.setText(finalValue);
                    // member = ss[3] + ":MEMBER CARD";
                    // cardType = "MEMBER CARD";
                    // order.setVisibility(View.VISIBLE);
                }

            } else {
                memberAn.setText("Error:" + finalValue);
                cardvalue.setText(finalValue);
                //member = "";
                // order.setVisibility(View.INVISIBLE);
            }
            mReader.piccPowerOff();
            mReader.sleep();
        }
        return "";
    }
    }
